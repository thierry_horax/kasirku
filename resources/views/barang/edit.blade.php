@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Edit Barang</h3>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form class="form_input">
                        @csrf
                         
                        <label class="form-control-label" for="kodeBarang">Kode Barang</label>
                        <input type="text" name="kodeBarang" id="kodeBarang" class="form-control" placeholder="Kode Barang" value="{{$data->kode_barang}}" required>
                        <label class="form-control-label" for="namaBarang">Nama Barang</label>
                        <input type="text" name="namaBarang" id="namaBarang" class="form-control" placeholder="Nama Barang" value="{{$data->nama_barang}}" required>
                        <label class="form-control-label" for="hargaBeli">Harga Beli</label>
                        <input type="text" name="hargaBeli" id="hargaBeli" class="form-control" placeholder="Harga Beli" value="{{$data->harga_beli}}" required>
                        <label class="form-control-label" for="hargaJual">Harga Jual</label>
                        <input type="text" name="hargaJual" id="hargaJual" class="form-control" placeholder="Harga Jual" value="{{$data->harga_jual}}" required>
                        <label class="form-control-label" for="ongkosKirim">Ongkos Kirim</label>
                        <input type="text" name="ongkosKirim" id="ongkosKirim" class="form-control" placeholder="Ongkos Kirim" value="{{$data->ongkos_kirim}}" required>
                        <label class="form-control-label" for="komisi">Komisi</label>
                        <input type="text" name="komisi" id="komisi" class="form-control" placeholder="Komisi" value="{{$data->komisi}}" required>
                        <label class="form-control-label" for="idKategori">Kategori</label>
                        <select class="form-control" name="idKategori" id="idKategori"  required>
                            @foreach ($kategoris as $kategory)
                                @if ($data->id_kategori == $kategory->id_kategori)
                                <option value="{{$kategory->id_kategori}}" selected>{{$kategory->nama_kategori}}</option>
                                @else
                                <option value="{{$kategory->id_kategori}}">{{$kategory->nama_kategori}}</option>
                                @endif
                            @endforeach
                        </select>
                        <label class="form-control-label" for="idSupplier">Supplier</label>
                        <select class="form-control" name="idSupplier" id="idSupplier"  required>
                            @foreach ($suppliers as $suppliery)
                            <option value="{{$suppliery->id_supplier}}" {{($data->id_supplier == $suppliery->id_supplier) ? "selected" : ""}}>{{$suppliery->nama_supplier}}</option>
                            @endforeach
                        </select>
                        <br>
                        <input type="submit" onclick="clicked()" class = "form-control btn btn-primary" value="SUBMIT">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function clicked()
    {
        if($('#kodeBarang').val()!='' && $('#namaBarang').val()!=''&& $('#hargaBeli').val()!='' && $('#hargaJual').val()!=''&& $('#ongkosKirim').val()!=''&& $('#komisi').val()!=''){
            //FUNCTION create
            $.ajax({
                type:"post",
                url:'{{route("barang.update", $data->id_barang)}}',
                headers:{
                    "X-CSRF-TOKEN" : "{{csrf_token()}}",
                    Authorization : "Bearer "+$('#_access_token').val()
                },
                data:{
                    kodeBarang : $('#kodeBarang').val(),
                    namaBarang : $('#namaBarang').val(),
                    hargaBeli : $('#hargaBeli').val(),
                    hargaJual : $('#hargaJual').val(),
                    ongkosKirim : $('#ongkosKirim').val(),
                    komisi : $('#komisi').val(),
                    idKategori : $('#idKategori').val(),
                    idSupplier : $('#idSupplier').val()
                },
                dataType : "json",
                statusCode : {
                    200: function(){
                        window.location.href="{{ route('barang') }}";
                    },
                    500: function (response) { 
                        alert(response.responseJSON.message);
                    }
                }
            });
        }
    }
</script>
@endsection
