<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <h1>Kasirku</h1>
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Master Data</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link {{(request()->routeIs('barang') || request()->routeIs('addbarang') || request()->routeIs('editbarang')) ? "active" : ""}}" href="{{ route('barang') }}">
                  Master Barang
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(request()->routeIs('supplier') || request()->routeIs('addsupplier') || request()->routeIs('editsupplier')) ? "active" : ""}}" href="{{ route('supplier') }}">
                    Master Supplier
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{(request()->routeIs('sales') || request()->routeIs('addsales') || request()->routeIs('editsales')) ? "active" : ""}}" href="{{ route('sales') }}">
                    Master Sales
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{(request()->routeIs('kategori') || request()->routeIs('addkategori') || request()->routeIs('editkategori')) ? "active" : ""}}" href="{{ route('kategori') }}">
                    Master Kategori
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{(request()->routeIs('user.create') || request()->routeIs('user.edit') || request()->routeIs('user.index')) ? "active" : ""}}" href="{{ route('user.index') }}">
                    Master User
                </a>
            </li>
          </ul>
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Penjualan & Pembelian</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link {{(request()->routeIs('penjualan.index') || request()->routeIs('penjualan.create') || request()->routeIs('penjualan.edit')) ? "active" : ""}}" href="{{ route('penjualan.index') }}">
                  Nota Penjualan
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(request()->routeIs('pembelian.index') || request()->routeIs('pembelian.create') || request()->routeIs('pembelian.edit')) ? "active" : ""}}" href="{{ route('pembelian.index') }}">
                    Nota Pembelian
                </a>
            </li>
          </ul>
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Laporan</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link {{(request()->routeIs('report.laporantransaksi')) ? "active" : ""}}" href="{{ route('report.laporantransaksi') }}">
                  Laporan Transaksi 
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{(request()->routeIs('report.laporankeuntungan')) ? "active" : ""}}" href="{{ route('report.laporankeuntungan') }}">
                    Laporan Keuntungan
                </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>