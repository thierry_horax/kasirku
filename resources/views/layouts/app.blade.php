<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Kasirku</title>
        <!-- Favicon -->
        <link href="{{ asset('img/brand/favicon.png') }}" rel="icon" type="image/png">
        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> -->
        <!-- Extra details for Live View on GitHub Pages -->

        <!-- Icons -->
        <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
        {{-- <link href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet"> --}}
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('css/argon.css?v=1.3.0') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('css/argon.css') }}" rel="stylesheet">

        @yield('custom_css')
    </head>
    <body class="{{ $class ?? '' }}">
        @auth
        <input type="hidden" id="_access_token" value="{{Session::get(Auth::user()->email.'_access_token')}}">
        @endauth
        
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @include('layouts.navbars.sidebar')
        @endauth
        
        <div class="main-content">
            @include('layouts.navbars.navbar')
            @yield('content')
        </div>

        @guest()
            @include('layouts.footers.guest')
        @endguest

        <script src=" {{asset('../public/vendor/jquery/dist/jquery.min.js')}}"></script>
        <script src=" {{asset('../public/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src=" {{asset('../public/vendor/js-cookie/js.cookie.js')}}"></script>
        <script src=" {{asset('../public/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
        <script src=" {{asset('../public/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
        <script src=" {{asset('../public/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
        <script src=" {{asset('../public/js/argon.js?v=1.2.0') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-3.5.1.js') }}"></script>

        <script>
            $(".form_input").submit(function (e) { 
                e.preventDefault();
                
            });
        </script>

        @yield('custom_js')
        @stack('js')
        
        <!-- Argon JS -->
        
    </body>
</html>