@extends('layouts.app')

@section('custom_css')
<link type="text/css" href="{{ asset('css/datatable.css') }}" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid ">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Tanggal</h3>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                   <div class="row mb-1">
                    <div class="col-md-4">
                        <label for="">Tanggal Awal</label>
                        <input type="text" class="form-control tanggal_awal">
                    </div>
                    <div class="col-md-4">
                        <label for="">Tanggal Akhir</label>
                        <input type="text" class="form-control tanggal_akhir">
                       </div>
                   </div>
                   <hr>
                   <div class="row mb-1">
                       <div class="col-2">
                        <button class="btn-primary btn" onclick="tampilkanReport()">Tampilkan Laporan</button>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Laporan Keuntungan</h3>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush tbl_laporan_keuntungan">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No Nota</th>
                                            <th>Tanggal</th>
                                            <th>Nama Barang</th>
                                            <th>Satuan</th>
                                            <th>Qty</th>
                                            <th>Harga Jual</th>
                                            <th>Harga Beli</th>
                                            <th>Laba/Rugi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<script src=" {{asset('../public/js/datatable.js')}}"></script>
<script src=" {{asset('../public/js/momentjs.min.js')}}"></script>
<script src=" {{asset('../public/js/currency.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.print.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    let dt = "";
    function tampilkanReport()
    {
        dt.clear().draw();
        
        let tanggalAwal = $(".tanggal_awal").val();
        let tanggalAkhir = $(".tanggal_akhir").val();
        $.ajax({
            type:"post",
            url:"{{route('report.getreportkeuntungan')}}",
            headers:{
                "X-CSRF-TOKEN" : "{{csrf_token()}}",
                Authorization : "Bearer " + $('#_access_token').val()
            },
            data : {
                tanggalAwal : tanggalAwal,
                tanggalAkhir : tanggalAkhir
            },
            dataType : 'json',
            statusCode : {
                200: function(response){
                    console.log(response);
                    let result = response.response;
                    result.forEach(barang => {
                        let string = `
                            <tr>
                                <td>${barang['no_nota']}</td>
                                <td>${barang['tanggal']}</td>
                                <td>${barang['nama_barang']}</td>
                                <td>${barang['satuan']}</td>
                                <td>${barang['qty']}</td>
                                <td>${currency(barang['harga_jual'], {separator : ","}).format().replace("$", 'Rp.')}</td>
                                <td>${currency(barang['harga_beli'], {separator : ","}).format().replace("$", 'Rp.')}</td>
                                <td>${currency(barang['harga_jual'] - barang['harga_beli'] , {separator : ","}).format().replace("$", 'Rp.')}</td>
                            </tr>
                        `
                        dt.rows.add($(string)).draw();
                    });
                    

                },
                500: function (response) { 
                    alert(response.responseJSON.message);
                 }
            }
        });
    }
    $(document).ready(function () {
        dt = $(".tbl_laporan_keuntungan").DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel','print'
            ]
        });

        $(".tanggal_awal").datepicker({
            dateFormat:"dd-mm-yy",
        });
        $(".tanggal_awal").val( moment().format('DD-MM-YYYY'));
        
        $(".tanggal_akhir").datepicker({
            dateFormat:"dd-mm-yy",
        });
        $(".tanggal_akhir").val(moment().format('DD-MM-YYYY'));
    });
</script>
@endsection
