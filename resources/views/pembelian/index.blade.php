@extends('layouts.app')

@section('custom_css')
<link type="text/css" href="{{ asset('css/datatable.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Daftar Nota Pembelian</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route('pembelian.create')}}" class="btn btn-sm btn-primary">+ Nota Pembelian</a>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush tbl_pembelian">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No. Nota</th>
                                            <th>Tanggal</th>
                                            <th>User</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     
                                    @foreach($listNotaPembelian as $nota)
                                        <tr>
                                            <td>{{ $nota->no_nota }}</td>
                                            <td>{{ date('d-m-Y', strtotime($nota->tanggal)) }}</td>
                                            <td>{{ $nota->name }}</td>
                                            <td>
                                            <button class="btn btn-warning btn-sm" onclick="openModalDetailPrint('{{$nota->no_nota}}')" data-toggle="modal" data-target="#printNota">Print Nota</button>
                                            <button class="btn btn-default btn-sm" onclick="window.location.href= '{{route('pembelian.edit', $nota->no_nota)}}'">Edit</button>
                                                <button class="btn btn-danger btn-sm" onclick="hapusNotaPembelian('{{$nota->no_nota}}')">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
        <div class="modal fade" id="printNota" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-xl" role="document">
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <h6 class="modal-title" id="modal-title-default"></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td >No. Nota</td>
                                            <td class="noNota"></td>
                                        </tr>
                                        <tr>
                                            <td >Tanggal</td>
                                            <td class="tanggal"></td>
                                        </tr>
                                        <tr>
                                            <td >Total </td>
                                            <td class="grandTotal"></td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush tbl_print_nota">
                                        <thead>
                                            <tr>
                                                <th>Kode Barang</th>
                                                <th>Nama Barang</th>
                                                <th>Supplier</th>
                                                <th>Satuan</th>
                                                <th>Jumlah</th>
                                                <th>Harga Satuan</th>
                                                <th>Diskon %</th>
                                                <th>SubTotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="modal-footer">
                        <a href="#" target="_blank"class="btn btn-primary print">Print</a>
                        <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src=" {{asset('../public/js/datatable.js')}}"></script>
<script src=" {{asset('../public/js/sweetalert.all.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src=" {{asset('../public/js/currency.js')}}"></script>
<script>
    let dt = "";
    $(document).ready(function () {
        $(".tbl_pembelian").DataTable();
        dt = $(".tbl_print_nota").DataTable({
            "dom": '<"top">rt<"bottom"p><"clear">'
        });
    });

    function hapusNotaPembelian(argNoNota) {
        let url = "{{route('pembelian.destroy', 'NO_NOTA')}}";
        url = url.replace("NO_NOTA", argNoNota);

        Swal.fire({
            title: `Peringatan!`,
            text: `Apakah anda yakin ingin menghapus ${argNoNota} ?`,
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Cancel',
            reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "post",
                        url: url,
                        headers:{
                            "X-CSRF-TOKEN" : "{{csrf_token()}}",
                            Authorization : "Bearer "+$('#_access_token').val()
                        },
                        statusCode : {
                            200: function(){
                                window.location.reload();
                            },
                            500: function (response) { 
                                alert(response.responseJSON.message);
                            }
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                )
                {
                    
                }
        })
        
    }
    
    function openModalDetailPrint(argNoNota) {
        $("#modal-title-default").html(`Print nota - ${argNoNota}`);
        getNotaDetail(argNoNota);

        // $("#printNota").modal('show');
    }

    function getNotaDetail(argNoNota){
        let grandTotal = 0;
        dt.clear().draw();

        let url = "{{route('pembelian.detail', 'NO_NOTA')}}";
        url =url.replace("NO_NOTA", argNoNota);

        $.ajax({
            type: "post",
            url: url,
            headers:{
                "X-CSRF-TOKEN" : "{{csrf_token()}}",
                Authorization : "Bearer "+$('#_access_token').val()
            },
            statusCode : {
                200: function(response){
                    let url_print = "{{route('pembelian.print', 'NO_NOTA')}}";
                    url_print = url_print.replace("NO_NOTA", argNoNota);
                    let result = response.response;
                    result.forEach(notaDetail => {
                        let string = `
                            <tr>
                                <td>${notaDetail['kode_barang']}</td>
                                <td>${notaDetail['nama_barang']}</td>
                                <td>${notaDetail['nama_supplier']}</td>
                                <td>${notaDetail['satuan']}</td>
                                <td>${notaDetail['qty']}</td>
                                <td>${currency(notaDetail['harga_satuan'], {separator : ","}).format().replace("$", "").replace(".00",'')}</td>
                                <td>${notaDetail['diskon']}</td>
                                <td>Rp. ${currency(notaDetail['subtotal'], {separator : ","}).format().replace("$", "").replace(".00",'')}</td>
                            </tr>
                        `
                        dt.rows.add($(string)).draw();
                        grandTotal += parseInt(notaDetail['subtotal']);
                        
                    });

                    $(".noNota").html(`: ${argNoNota}`);
                    $(".tanggal").html(`: ${result[0]['tanggal']}`);
                    $(".grandTotal").html(`: Rp. ${currency(grandTotal, {separator : ","}).format().replace("$", "").replace(".00",'')}`);
                    $(".print").attr('href', url_print);
                },
                500: function (response) { 
                    alert(response.responseJSON.message);
                }
            }
        })
    }
</script>
@endsection
