<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="style.css">
        <title>Print Nota</title>
        <style>
            * {
            font-size: 12px;
            font-family: 'Times New Roman';
            }

            td,
            th,
            tr,
            table {
                border-top: 1px solid black;
                border-collapse: collapse;
            }

            td.description,
            th.description {
                width: 75px;
                max-width: 75px;
            }

            td.quantity,
            th.quantity {
                width: 50px;
                max-width: 50px;
                word-break: break-all;
            }

            td.price,
            th.price {
                width: 40px;
                max-width: inherit;
                /* word-break: break-all; */
            }

            .centered {
                text-align: center;
                align-content: center;
            }

            .ticket {
                width: 155px;
                max-width: 155px;
            }

            img {
                max-width: inherit;
                width: inherit;
            }

            @media print {
                .hidden-print,
                .hidden-print * {
                    display: none !important;
                }
            }
        </style>
    </head>
    <body>
        <div class="ticket">
            <p class="centered"><label for="" style="font-size: 16pt;">Kasirku</label>
                <br>Alamat
                <br>notelepon</p>
                --------------------------------------
            <br>
            <label for="">No. Nota </label>
            <label style="text-align: right; align-content: flex-end">: {{$notaDetail[0]->no_nota}}</label> <br>
            <label for="">Tanggal </label> &nbsp;
            <label>: {{date('d-m-Y', strtotime($notaDetail[0]->tanggal))}}</label><br>
            <label for="">Supplier </label>&nbsp;
            <label>: {{$notaDetail[0]->nama_supplier}}</label>
            <table>
                <thead>
                    <tr>
                        <th class="quantity">Qty.</th>
                        <th class="description">Description</th>
                        <th class="price">Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($notaDetail as $barang)
                    <tr>
                        <td class="quantity">{{$barang->qty}}</td>
                        <td class="description">{{$barang->nama_barang}}</td>
                        <td class="price">{{number_format($barang->subtotal)}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="quantity"></td>
                        <td class="description">TOTAL</td>
                    <td class="price">{{number_format($total)}}</td>
                    </tr>
                </tbody>
            </table>
            <p class="centered">Terima kasih!</p>
        </div>
    </body>
    <script>
        window.print();
    </script>
</html>