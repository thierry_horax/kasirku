@extends('layouts.app')

@section('custom_css')
<link type="text/css" href="{{ asset('css/datatable.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Master Supplier</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('addsupplier') }}" class="btn btn-sm btn-primary">+ Supplier</a>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush tbl_supplier">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Id Supplier</th>
                                            <th>Nama Supplier</th>
                                            <th>Nomor Telepon</th>
                                            <th>Alamat</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     
                                    @foreach($data as $datas)
                                        <tr>
                                            <td>{{ $datas->id_supplier }}</td>
                                            <td>{{ $datas->nama_supplier }}</td>
                                            <td>{{ $datas->no_telepon }}</td>
                                            <td>{{ $datas->alamat }}</td>
                                            <td>
                                                <a class="btn btn-default" href="{{route('editsupplier', $datas->id_supplier)}}">Edit</a>
                                                <button class="btn btn-danger" onclick="clicked({{ $datas->id_supplier }}, '{{ $datas->nama_supplier }}')">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function clicked(argIdSupplier, argNamaSupplier)
    {
        let url = "{{route('supplier.destroy', 'ID_SUPPLIER')}}";
        url = url.replace('ID_SUPPLIER', argIdSupplier);
        Swal.fire({
            title: `Peringatan!`,
            text: `Apakah anda yakin ingin menghapus ${argNamaSupplier} ?`,
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Cancel',
            reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type:"post",
                        url:url,
                        headers:{
                            "X-CSRF-TOKEN" : "{{csrf_token()}}",
                            Authorization : "Bearer "+$('#_access_token').val()
                        },
                        statusCode : {
                            200: function(){
                                window.location.href="{{ route('supplier') }}";
                            },
                            500: function (response) { 
                                alert(response.responseJSON.message);
                            }
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                )
                {
                    
                }
        })
        //FUNCTION destroy
        
    }
</script>
@endsection
@section('custom_js')
<script src=" {{asset('../public/js/datatable.js')}}"></script>
<script src=" {{asset('../public/js/sweetalert.all.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<script>
    $(document).ready(function () {
        $(".tbl_supplier").DataTable();
    });
</script>
@endsection
