@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Add Supplier</h3>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form class="form_input">
                         
                        <label class="form-control-label" for="namaSupplier">Nama Supplier</label>
                        <input type="text" name="namaSupplier" id="namaSupplier" class="form-control" placeholder="Nama Supplier" required>
                        <label class="form-control-label" for="noTelepon">No Telepon</label>
                        <input type="number" name="noTelepon" id="noTelepon" class="form-control" placeholder="No Telepon" required>
                        <label class="form-control-label" for="alamat">Alamat</label>
                        <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat" required>
                        <br>
                        <input type="submit" onclick="clicked()" class = "form-control btn btn-primary" value="SUBMIT">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function clicked()
    {
        if($('#namaSupplier').val()!='' && $('#noTelepon').val()!=''&& $('#alamat').val()!='' ){
            //FUNCTION create
            $.ajax({
                type:"post",
                url:'{{route("supplier.store")}}',
                headers:{
                    "X-CSRF-TOKEN" : "{{csrf_token()}}",
                    Authorization : "Bearer "+$('#_access_token').val()
                },
                data:{
                    namaSupplier : $('#namaSupplier').val(),
                    noTelepon : $('#noTelepon').val(),
                    alamat : $('#alamat').val()
                },
                dataType : "json",
                statusCode : {
                    200: function(){
                        window.location.href="{{ route('supplier') }}";
                    },
                    500: function (response) { 
                        alert(response.responseJSON.message);
                    }
                }
            });
        }
    }
</script>
@endsection
