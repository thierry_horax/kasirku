@extends('layouts.app')

@section('custom_css')
<link type="text/css" href="{{ asset('css/datatable.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href=" {{asset('../public/css/themes/default.date.css')}}">
<link rel="stylesheet" href=" {{asset('../public/css/themes/pickadate.default.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Add Nota Penjualan</h3>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form class="form_input">
                        @csrf
                         

                        <label class="form-control-label" for="noNota">No. Nota</label>
                        <input type="text" name="noNota" id="noNota" class="form-control mb-1" placeholder="No Telepon"  value="{{$notaPenjualan[0]->no_nota}}" readonly>

                        <label class="form-control-label" for="Tanggal">Tanggal</label>
                        <input type="text" name="Tanggal" id="tanggal" class="form-control mb-1" placeholder="Tanggal" value="{{date('d-m-Y', strtotime($notaPenjualan[0]->tanggal))}}">

                        <label class="form-control-label" for="namaSales">Daftar Master Barang</label>
                        <div class="row">
                            <input type="hidden" class="idBarang">
                            <div class="col-sm-2 mb-1">
                                <input type="text" class="form-control kodeBarang" placeholder="Kode Item">
                            </div>
                            <div class="col-sm-4 mb-1">
                                <input type="text" class="form-control namaBarang" placeholder="Nama Item" readonly>
                            </div>
                            <div class="col-sm-1 mb-1">
                                <input type="text" class="form-control satuan" placeholder="Satuan">
                            </div>
                            <div class="col-sm-1">
                                <input type="number" class="form-control hargaSatuan" placeholder="Harga" min="1">
                            </div>
                            <div class="col-sm-1">
                                <input type="number" class="form-control qty" placeholder="Qty" min="1">
                            </div>
                            <div class="col-sm-1">
                                <input type="number" class="form-control diskon" placeholder="Diskon %" min="1" step="any">
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn-primary form-control" onclick="addItemToTabel()">+ Masukkan ke tabel barang</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <label class="form-control-label" for="namaSales">Tabel barang</label>
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush tbl_daftar_barang">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Kode Barang</th>
                                                <th>Nama Barang</th>
                                                <th>Satuan</th>
                                                <th>Jumlah</th>
                                                <th>Harga Satuan (Rp.)</th>
                                                <th>Diskon</th>
                                                <th>Subtotal (Rp.)</th>
                                                <th>Action</th>
                                                {{-- <th>Opsi</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($notaPenjualan as $barang)
                                            <tr>
                                                <td>{{$barang->kode_barang}}</td>
                                                <td>{{$barang->nama_barang}}</td>
                                                <td>{{$barang->satuan}}</td>
                                                <td>{{$barang->qty}}</td>
                                                <td>{{number_format($barang->harga_satuan)}}</td>
                                                <td>{{$barang->diskon}}</td>
                                                <td>{{number_format($barang->subtotal)}}</td>
                                                <td>
                                                    <button type="button" class="btn-icon btn-sm btn-danger btn-primary"  onclick="deleteItemFromCollection(this, {{$loop->index + 1}})">
                                                        <i class="ni ni-fat-remove"></i>    
                                                    </button>    
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4" />
                        <div class="row">
                            <div class="col-sm-7">
                                <h2>Total</h2>
                            </div>
                            <div class="col-sm-5">
                                <h2>Rp. <label class="grandTotal">0</label></h2>
                            </div>
                        </div>
                        <br>
                    <input type="submit" onclick="simpanNotaPembelian('{{$notaPenjualan[0]->no_nota}}')" class = "form-control btn btn-primary" value="SUBMIT">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script src=" {{asset('../public/js/datatable.js')}}"></script>
<script src=" {{asset('../public/js/select2.min.js')}}"></script>
<script src=" {{asset('../public/js/picker.js')}}"></script>
<script src=" {{asset('../public/js/picker.date.js')}}"></script>
<script src=" {{asset('../public/js/currency.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    let dt = "";
    let listBarang = [];

    function simpanNotaPembelian(argNoNota)
    {
        let url = "{{route('penjualan.update', 'NO_NOTA')}}";
        url = url.replace("NO_NOTA", argNoNota);
        //FUNCTION create
        if(listBarang.length != 0)
        {
            $.ajax({
                type:"post",
                url: url,
                headers:{
                    "X-CSRF-TOKEN" : "{{csrf_token()}}",
                    Authorization : "Bearer "+$('#_access_token').val()
                },
                data:{
                    noNota : $('#noNota').val(),
                    tanggal : $('#tanggal').val(),
                    listBarang : JSON.stringify(listBarang)
                },
                dataType : "json",
                statusCode : {
                    200: function(){
                        window.location.href="{{ route('penjualan.index') }}";
                    },
                    500: function (response) { 
                        alert(response.responseJSON.message);
                    }
                }
            });
        }
        else{
            alert('Tabel barang masih kosong');
        }
    }

    function addItemToTabel() {
        let idBarang = $(".idBarang").val();
        let qty = $(".qty").val();
        let hargaSatuan = $(".hargaSatuan").val().replace(',', '');
        let satuan = $(".satuan").val();
        let url = "{{route('barang.getbarangdetail', 'ID_BARANG')}}";
        let diskon = ($(".diskon").val() == "" || $(".diskon").val() == undefined) ? 0 : $(".diskon").val();
        url = url.replace("ID_BARANG", idBarang);
        $.ajax({
            type: "get",
            url: url,
            headers : {
                "Authorization" : `Bearer ${$('#_access_token').val()}`
            },
            statusCode :{
                200:function(response) {
                    let barangDetail = response.response[0];
                    let index =  (listBarang.length != 0) ? listBarang[listBarang.length-1]['index'] + 1 : 1;
                    let subtotal = qty * hargaSatuan;
                    let potonganDiskon = parseFloat(diskon)/100 * subtotal;
                    subtotal = subtotal - potonganDiskon;
                    let barangDetailString = `
                        <tr>
                            <td>${barangDetail['kode_barang']}</td>
                            <td>${barangDetail['nama_barang']}</td>
                            <td>${satuan}</td>
                            <td>${qty}</td>
                            <td>${currency(hargaSatuan, {separator : ","}).format().replace("$", "").replace(".00",'')}</td>
                            <td>${diskon}</td>
                            <td>${currency(subtotal, {separator : ","}).format().replace("$", "").replace(".00",'')}</td>
                            <td>
                                <button type="button" class="btn-icon btn-sm btn-danger btn-primary" onclick="deleteItemFromCollection(this, ${index})">
                                    <i class="ni ni-fat-remove"></i>    
                                </button>    
                            </td>
                        </tr>
                    `
                    listBarang.push({'idBarang' : barangDetail['id_barang'], 'qty' : qty, 'subtotal' : subtotal, 'index' : index, 'hargaSatuan' : hargaSatuan, 'diskon' : diskon, 'satuan' : satuan})

                    dt.rows.add($(barangDetailString)).draw();
                
                    $(".qty").val("");
                    $(".hargaSatuan").val("");
                    $(".kodeBarang").val("");
                    $(".namaBarang").val("");
                    $(".diskon").val("");
                    $(".satuan").val("");
                    updateGrandTotal();
                },
                500:function(response){
                    alert(response.responseJSON.message);
                }
            }
        });
    }

    function deleteItemFromCollection(argObjectButton, argIndex) {
        let row = $(argObjectButton).closest('tr')[0];
        dt.rows($(row)).remove().draw();

        listBarang.forEach(barang => {
            console.log(barang.index);
            if(barang.index == argIndex)
            {
                listBarang.splice(argIndex-1,1);
                updateGrandTotal();
                return;
            }
        });
    }

    function updateGrandTotal() {
        let grandTotal = 0;
        listBarang.forEach(barang => {
            grandTotal += parseInt(barang.subtotal);
        });

        $(".grandTotal").html(currency(grandTotal, {separator : ","}).format().replace("$", "").replace(".00",''));
    }

    $(document).ready(function () {
        listBarang = {!!json_encode($listBarangInNotaPenjualan)!!};
        dt = $(".tbl_daftar_barang").DataTable({
            "dom": '<"top">rt<"bottom"p><"clear">'
        });
        updateGrandTotal();
        $(".masterBarang").select2();
        $("#tanggal").datepicker({
            dateFormat:"dd-mm-yy",
        });
    });

    function cariBarang(argKodeBarang){
        $.ajax({
            type: "post",
            url: "{{route('barang.caribarang')}}",
            headers : {
                "X-CSRF-TOKEN" : "{{csrf_token()}}",
                "Authorization" : `Bearer ${$('#_access_token').val()}`
            },
            data :{
                "kodeBarang" : argKodeBarang
            },
            dataType : "json",
            statusCode :{
                200:function(response) {
                    console.log(response);
                    if(response.response.length > 0)
                    {
                        
                        $(".idBarang").val(response.response[0].id_barang);
                        $(".namaBarang").val(response.response[0].nama_barang);
                    }
                },
                500:function(response){
                    alert(response.responseJSON.message);
                }
            }
        });
   }

   $(".kodeBarang").keyup(function (e) { 
       cariBarang(this.value);
    })
</script>
@endsection
