@extends('layouts.app')

@section('custom_css')
<link type="text/css" href="{{ asset('css/datatable.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Master Kategori</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('addkategori')}}" class="btn btn-sm btn-primary">+ Kategori</a>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush tbl_kategori">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Id Kategori</th>
                                            <th>Nama Kategori</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     
                                    @foreach($data as $datas)
                                        <tr>
                                            <td>{{ $datas->id_kategori }}</td>
                                            <td>{{ $datas->nama_kategori }}</td>
                                            <td>
                                                <a class="btn btn-default" href="{{route('editkategori', $datas->id_kategori)}}">Edit</a>
                                                <button class="btn btn-danger" onclick="clicked({{$datas->id_kategori}}, '{{ $datas->nama_kategori }}')">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function clicked(argIdKategori, argNamaKategori)
    {
        let url = "{{route('kategori.destroy', 'ID_KATEGORI')}}";
        url = url.replace('ID_KATEGORI', argIdKategori);
        Swal.fire({
            title: `Peringatan!`,
            text: `Apakah anda yakin ingin menghapus ${argNamaKategori} ?`,
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Cancel',
            reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type:"post",
                        url: url,
                        headers:{
                            "X-CSRF-TOKEN" : "{{csrf_token()}}",
                            Authorization : "Bearer "+$('#_access_token').val()
                        },
                        statusCode : {
                            200: function(){
                                window.location.href="{{ route('kategori') }}";
                            },
                            500: function (response) { 
                                alert(response.responseJSON.message);
                            }
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                )
                {
                    
                }
        })
        //FUNCTION destroy
        
    }
</script>
@endsection

@section('custom_js')
<script src=" {{asset('../public/js/datatable.js')}}"></script>
<script src=" {{asset('../public/js/sweetalert.all.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script>
    $(document).ready(function () {
        $(".tbl_kategori").DataTable();
    });
</script>
@endsection

