@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Edit Kategori</h3>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form_input">
                        <label class="form-control-label" for="namaKategori">Nama Kategori</label>
                        <input type="text" name="namaKategori" id="namaKategori" class="form-control" placeholder="Nama Kategori" value="{{$data->nama_kategori}}" required>
                        <br>
                        <input type="submit" onclick="clicked()" class = "form-control btn btn-primary" value="SUBMIT">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function clicked()
    {
        if($('#namaKategori').val()!=''){
            //FUNCTION update
            $.ajax({
                type:"post",
                url:'{{route("kategori.update", $data->id_kategori)}}',
                headers:{
                    "X-CSRF-TOKEN" : "{{csrf_token()}}",
                    Authorization : "Bearer "+$('#_access_token').val()
                },
                data:{
                    namaKategori : $('#namaKategori').val()
                },
                dataType : "json",
                statusCode : {
                    200: function(){
                        window.location.href="{{ route('kategori') }}";
                    },
                    500: function (response) { 
                        alert(response.responseJSON.message);
                    }
                }
            });
        }
    }
</script>
@endsection
