-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2020 at 06:27 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasirku`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1011775678b9f0477adf06e08c46915a3d536476d2790847d109824012db9a4ac914944b8adc1882', 1, 1, NULL, '[]', 0, '2020-09-17 04:05:20', '2020-09-17 04:05:20', '2021-09-17 11:05:20'),
('69d049056d5e1f9033a3d82ca8da85aabef08e49af5f993afd22d7a241253bd2e3025a9640ad87e4', 1, 1, NULL, '[]', 0, '2020-09-25 03:05:34', '2020-09-25 03:05:34', '2021-09-25 10:05:34'),
('a353f3471b5f9439e3d3f24aa0a94a37020538c0366a218bdf0703062ae8a1f4fd68cede925606c4', 1, 1, NULL, '[]', 0, '2020-09-25 03:17:53', '2020-09-25 03:17:53', '2021-09-25 10:17:53'),
('d54f7b052ccbaf0bad0a05719d12629baa856622a4dfd5ebc15d637e44aedebbb9b11599f9f616d7', 1, 1, NULL, '[]', 0, '2020-09-19 05:23:35', '2020-09-19 05:23:35', '2021-09-19 12:23:35');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'LfZTO8WvwJcimRVUOCFSC3SoVbxis5pceTh8RLJd', NULL, 'http://localhost', 1, 0, 0, '2020-09-17 04:05:08', '2020-09-17 04:05:08'),
(2, NULL, 'Laravel Password Grant Client', 'QC0eoWEjKWZ6NCCU6miw0Q7FdXti2w6T0spKVdid', 'users', 'http://localhost', 0, 1, 0, '2020-09-17 04:05:08', '2020-09-17 04:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-17 04:05:08', '2020-09-17 04:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(45) DEFAULT NULL,
  `nama_barang` varchar(200) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `ongkos_kirim` int(11) DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `kode_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `ongkos_kirim`, `komisi`, `id_kategori`, `id_supplier`) VALUES
(1, 'BAJU0001', 'Baju Sekolah', 100000, 100000, 100000, 100000, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`) VALUES
(2, 'Baju Kaos'),
(4, 'Sepatu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_pembelian`
--

CREATE TABLE `tbl_nota_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_pembelian`
--

INSERT INTO `tbl_nota_pembelian` (`id_pembelian`, `no_nota`, `tanggal`, `id_user`) VALUES
(1, 'NB190920200001', '2020-09-19 00:00:00', '1'),
(4, 'NB250920200002', '2020-09-25 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_pembelian_detail`
--

CREATE TABLE `tbl_nota_pembelian_detail` (
  `id_pembelian_detail` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `harga_satuan` int(11) DEFAULT NULL,
  `diskon` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_pembelian_detail`
--

INSERT INTO `tbl_nota_pembelian_detail` (`id_pembelian_detail`, `id_pembelian`, `id_barang`, `qty`, `subtotal`, `harga_satuan`, `diskon`) VALUES
(2, 1, 1, 2, 24624624, NULL, NULL),
(3, 1, 1, 3, 36936936, NULL, NULL),
(7, 4, 1, 20, 140000, 10000, 30),
(8, 4, 1, 50, 4500000, 100000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_penjualan`
--

CREATE TABLE `tbl_nota_penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_penjualan`
--

INSERT INTO `tbl_nota_penjualan` (`id_penjualan`, `no_nota`, `tanggal`, `id_user`) VALUES
(1, 'NJ190920200001', '2020-09-19 00:00:00', '1'),
(2, 'NJ190920200002', '2020-09-19 00:00:00', '1'),
(3, 'NJ250920200003', '2020-09-25 00:00:00', '1'),
(4, 'NJ250920200004', '2020-09-25 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_penjualan_detail`
--

CREATE TABLE `tbl_nota_penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `harga_satuan` int(11) DEFAULT NULL,
  `diskon` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_penjualan_detail`
--

INSERT INTO `tbl_nota_penjualan_detail` (`id_penjualan_detail`, `id_penjualan`, `id_barang`, `qty`, `subtotal`, `harga_satuan`, `diskon`) VALUES
(18, 3, 1, 5, 300000, 100000, 40),
(19, 3, 1, 200, 200000, 5000, 80),
(20, 3, 1, 5, 450000, 90000, 0),
(21, 3, 1, 1, 54500, 100000, 45.5),
(22, 4, 1, 1, 197500, 500000, 60.5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales`
--

CREATE TABLE `tbl_sales` (
  `id_sales` int(11) NOT NULL,
  `nama_sales` varchar(200) DEFAULT NULL,
  `no_telepon` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sales`
--

INSERT INTO `tbl_sales` (`id_sales`, `nama_sales`, `no_telepon`, `alamat`) VALUES
(1, 'Calvin', '123123123123', 'Jalan Siwalankerto'),
(2, 'Thierry Horax', '085102878123', 'asdqwdqw12312'),
(7, 'maharani', '098765432', '123125123123');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(200) DEFAULT NULL,
  `no_telepon` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`id_supplier`, `nama_supplier`, `no_telepon`, `alamat`) VALUES
(1, 'Thierry', '0987654321', 'Jalan Siwalankerto Permai 1 blok A7');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `jabatan`) VALUES
(1, 'Thierry Horax', 'thierryhorax', NULL, '$2y$10$exAjPUNQbKPZTU81AwBgYOORz/6FtxFBt1MusmSl1zVTYReyO/yqS', NULL, '2020-09-16 19:58:11', '2020-09-16 19:58:11', 'owner'),
(23, 'Kinkin', 'kinkin', NULL, '', NULL, NULL, NULL, 'Sales'),
(24, 'Kinkin', 'kinkin406', NULL, '', NULL, NULL, NULL, 'Sales'),
(25, 'maharani Kurnia', 'maharani', NULL, '$2y$10$m2Ge2frseycTlV2TcK1sJe1aY1/1ykUhuL/4MWA91xNuHC2IYwm0y', NULL, NULL, NULL, 'Sales'),
(26, 'Kinkin', 'kinkin430', NULL, '', NULL, NULL, NULL, 'Sales'),
(27, 'Calvin', 'calvinlie', NULL, '', NULL, '2020-09-19 09:40:03', '2020-09-19 09:40:03', 'Owner');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `fk_tbl_barang_tbl_kategori1_idx` (`id_kategori`),
  ADD KEY `fk_tbl_barang_tbl_supplier1_idx` (`id_supplier`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_nota_pembelian`
--
ALTER TABLE `tbl_nota_pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_detail`,`id_pembelian`,`id_barang`),
  ADD KEY `fk_tbl_pembelian_has_tbl_barang_tbl_barang1_idx` (`id_barang`),
  ADD KEY `fk_tbl_pembelian_has_tbl_barang_tbl_pembelian_idx` (`id_pembelian`);

--
-- Indexes for table `tbl_nota_penjualan`
--
ALTER TABLE `tbl_nota_penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  ADD PRIMARY KEY (`id_penjualan_detail`,`id_penjualan`,`id_barang`),
  ADD KEY `fk_tbl_penjualan_has_tbl_barang_tbl_barang1_idx` (`id_barang`),
  ADD KEY `fk_tbl_penjualan_has_tbl_barang_tbl_penjualan1_idx` (`id_penjualan`);

--
-- Indexes for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_nota_pembelian`
--
ALTER TABLE `tbl_nota_pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_nota_penjualan`
--
ALTER TABLE `tbl_nota_penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  MODIFY `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD CONSTRAINT `fk_tbl_barang_tbl_kategori1` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_barang_tbl_supplier1` FOREIGN KEY (`id_supplier`) REFERENCES `tbl_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  ADD CONSTRAINT `fk_tbl_pembelian_has_tbl_barang_tbl_barang1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_pembelian_has_tbl_barang_tbl_pembelian` FOREIGN KEY (`id_pembelian`) REFERENCES `tbl_nota_pembelian` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  ADD CONSTRAINT `fk_tbl_penjualan_has_tbl_barang_tbl_barang1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_penjualan_has_tbl_barang_tbl_penjualan1` FOREIGN KEY (`id_penjualan`) REFERENCES `tbl_nota_penjualan` (`id_penjualan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
