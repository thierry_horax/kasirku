-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 10, 2020 at 12:29 PM
-- Server version: 10.2.33-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thierry1_kasirku`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('19a51433bde14e086d0c6d6dea2f655eda03d9702d83c6506818ffd29eca09e70d46decbf6ffd03b', 1, 9, NULL, '[]', 0, '2020-09-22 19:29:46', '2020-09-22 19:29:46', '2021-09-23 02:29:46'),
('1f52fcc1fa27ea2ebac6df28ea47662900a758c1d8ba8820b201ac4a5f63167cd40cdf6e27c4f1c7', 1, 3, NULL, '[]', 0, '2020-09-17 04:21:07', '2020-09-17 04:21:07', '2021-09-17 11:21:07'),
('27499716fec6625e7d472f2d07c68000b0c64b41add843b0b35687a574d607e3e488801981679095', 1, 5, NULL, '[]', 0, '2020-09-17 04:41:37', '2020-09-17 04:41:37', '2021-09-17 11:41:37'),
('3527394cce1c929bbeb706f612a5178b36c4c3d3c168060905790c8cc510ff8d4bdb8e57584c7970', 1, 9, NULL, '[]', 0, '2020-09-22 09:35:18', '2020-09-22 09:35:18', '2021-09-22 16:35:18'),
('35680ab638b9f57ca393c22abeb1865f52d6fb63460c81722853b5983ed8d4294f8fc784d501b64d', 1, 7, NULL, '[]', 0, '2020-09-19 05:17:16', '2020-09-19 05:17:16', '2021-09-19 12:17:16'),
('3a0eede050dd54f2af6c86388ded22cbd37f35dff88bace6d8d6090a2ea1a423f7ac46071433827f', 1, 7, NULL, '[]', 0, '2020-09-18 22:35:59', '2020-09-18 22:35:59', '2021-09-19 05:35:59'),
('5e3bb3f44be61db288ad1aff0fc07a31f9a633b30987e7cc71c4aecb997b8e496b11cd74693da647', 23, 7, NULL, '[]', 0, '2020-09-19 09:56:05', '2020-09-19 09:56:05', '2021-09-19 16:56:05'),
('5f44a8e5a9bf5678298ff14c9e71a41ea4ec9111d6af3317ae71712e2ae48841a01f74f764f776e4', 1, 7, NULL, '[]', 0, '2020-09-17 04:58:28', '2020-09-17 04:58:28', '2021-09-17 11:58:28'),
('605e7ebb8f0715a86a77bc2a28ba866652e1e730e6face2f3937e7064017775d6398feb83f22e868', 1, 1, NULL, '[]', 0, '2020-09-17 03:58:55', '2020-09-17 03:58:55', '2021-09-17 10:58:55'),
('648b0ad68406d6a0ea3f16c27bd718026294ecbacbd9806edf658aebbf3b98f45410168fc2dabd34', 1, 7, NULL, '[]', 0, '2020-09-17 20:44:34', '2020-09-17 20:44:34', '2021-09-18 03:44:34'),
('75f14115edeebe3f49c2c093e37973cdac39e246f78e44ae012ed3ea47a5d5a57b9dcb719a949f23', 1, 9, NULL, '[]', 0, '2020-10-03 02:14:32', '2020-10-03 02:14:32', '2021-10-03 09:14:32'),
('77addc2f0ddec9fbd6e55265e9f3a6fe08b0f5229d47ab952e526a30bab3966c05027cfcf2446b44', 1, 9, NULL, '[]', 0, '2020-10-07 02:15:09', '2020-10-07 02:15:09', '2021-10-07 09:15:09'),
('8087be81458a4ec84a200056c8922bcf672cac732fe060eef2f7cfea071bf8268589763db2e2fc73', 1, 7, NULL, '[]', 0, '2020-09-19 09:54:28', '2020-09-19 09:54:28', '2021-09-19 16:54:28'),
('856abecef9a8e0c2dd950d009dcd3b033ac9f30ec2fe60238561d612268b8adc3a8ae6379782c2fa', 1, 9, NULL, '[]', 0, '2020-09-19 10:14:49', '2020-09-19 10:14:49', '2021-09-19 17:14:49'),
('94a9c1773c013add180e23fe28a2fac9ea527c9378d9b313a00e9f2676eadc6c234100e0b204f36d', 1, 9, NULL, '[]', 0, '2020-09-19 23:21:48', '2020-09-19 23:21:48', '2021-09-20 06:21:48'),
('b3bcd88d7ff631c0f94d7954115e9f99373d15ca31e95898cb2d19810c7b37bbb86639b014c0747f', 1, 7, NULL, '[]', 0, '2020-09-17 06:02:49', '2020-09-17 06:02:49', '2021-09-17 13:02:49'),
('c21001baa5d480ea0336966cfd6687008d7ce0363ac29effcaf9dbe2fd8cc192b5a38f3551fbf44a', 1, 7, NULL, '[]', 0, '2020-09-19 09:56:22', '2020-09-19 09:56:22', '2021-09-19 16:56:22'),
('e02fb592a74029942c19b929f5aa6b6fb4d9b49dd63f1d29c55e120739eac6c63a329fcdf45935bc', 1, 7, NULL, '[]', 0, '2020-09-18 23:45:54', '2020-09-18 23:45:54', '2021-09-19 06:45:54'),
('e98aed478f6155408cb448b25d580d069233f157d85ad0433ee4181809d16d54e7fa079738b2dc93', 23, 7, NULL, '[]', 0, '2020-09-17 05:02:48', '2020-09-17 05:02:48', '2021-09-17 12:02:48'),
('e9dd9a4de79e054347c721b79d9fa0d4e47fecec6eaa9f8c168796048b30f7d1e5dc2e76ce61e5e7', 1, 7, NULL, '[]', 0, '2020-09-18 01:22:38', '2020-09-18 01:22:38', '2021-09-18 08:22:38'),
('f348dd7de7a49f541a73d5e016570a893836c12e27f86099212c4bd4da0cae7d1bf357c13a21fbc7', 1, 9, NULL, '[]', 0, '2020-09-26 23:31:06', '2020-09-26 23:31:06', '2021-09-27 06:31:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '3rXAwgxKSiMud4FD1vXvCAokhotQ20m78YuVl89G', NULL, 'http://localhost', 1, 0, 0, '2020-09-17 03:58:44', '2020-09-17 03:58:44'),
(2, NULL, 'Laravel Password Grant Client', 'fL4BUY7fcBavqHiks6778i2qrHHJMeZ8RBY7bMgm', 'users', 'http://localhost', 0, 1, 0, '2020-09-17 03:58:44', '2020-09-17 03:58:44'),
(3, NULL, 'Laravel Personal Access Client', 'hi6dIm6OdDgGvfU85yUs05vNYT4sjovgKa5nJ9Se', NULL, 'http://localhost', 1, 0, 0, '2020-09-17 04:20:52', '2020-09-17 04:20:52'),
(4, NULL, 'Laravel Password Grant Client', 'k5OVlKPUvmh9EbAKJGC5iGuArcCICafdyIdK4zq7', 'users', 'http://localhost', 0, 1, 0, '2020-09-17 04:20:52', '2020-09-17 04:20:52'),
(5, NULL, 'Laravel Personal Access Client', '5HFRY4mzXUTYjWunU68npXIzh6uAIswYd1r0hkRM', NULL, 'http://localhost', 1, 0, 0, '2020-09-17 04:40:42', '2020-09-17 04:40:42'),
(6, NULL, 'Laravel Password Grant Client', 'Vmt8uuWdBTHZZnSqYil2UhGT6AG28wbxQRt6bhJs', 'users', 'http://localhost', 0, 1, 0, '2020-09-17 04:40:42', '2020-09-17 04:40:42'),
(7, NULL, 'Laravel Personal Access Client', '9fx3josVEEVZxpfpbqIikzutYZizdhRPEbSbDkRm', NULL, 'http://localhost', 1, 0, 0, '2020-09-17 04:58:17', '2020-09-17 04:58:17'),
(8, NULL, 'Laravel Password Grant Client', 'AxVywzcIm8013j0fexhIeR88c4NTjvyfWxu6lybt', 'users', 'http://localhost', 0, 1, 0, '2020-09-17 04:58:17', '2020-09-17 04:58:17'),
(9, NULL, 'Laravel Personal Access Client', 'IRwaVXtDcA2B44WEt0ukF55RfrxKGUAYXQKIHo85', NULL, 'http://localhost', 1, 0, 0, '2020-09-19 10:11:41', '2020-09-19 10:11:41'),
(10, NULL, 'Laravel Password Grant Client', 'IGdOlIJX7ojQxklgtlMSfMbLDTzpEiDoaUIoivdO', 'users', 'http://localhost', 0, 1, 0, '2020-09-19 10:11:41', '2020-09-19 10:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-17 03:58:44', '2020-09-17 03:58:44'),
(2, 3, '2020-09-17 04:20:52', '2020-09-17 04:20:52'),
(3, 5, '2020-09-17 04:40:42', '2020-09-17 04:40:42'),
(4, 7, '2020-09-17 04:58:17', '2020-09-17 04:58:17'),
(5, 9, '2020-09-19 10:11:41', '2020-09-19 10:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(45) DEFAULT NULL,
  `nama_barang` varchar(200) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `ongkos_kirim` int(11) DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `kode_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `ongkos_kirim`, `komisi`, `id_kategori`, `id_supplier`) VALUES
(2, '11-00001', 'senar', 50000, 100000, 4000, 10, 2, 4),
(3, '11-00002', 'raket', 500000, 700000, 0, 10, 2, 4),
(4, '11-00003', 'baju', 100000, 90000, 0, 10, 2, 4),
(5, '11-00004', 'sepatu', 1000000, 1200000, 10000, 0, 2, 4),
(6, '11-00005', 'buku', 25000, 35000, 2000, 10, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inventory`
--

CREATE TABLE `tbl_inventory` (
  `id_inventory` int(11) NOT NULL,
  `kode_barang` varchar(45) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `tanggal_beli` datetime DEFAULT NULL,
  `tanggal_jual` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_penjualan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`) VALUES
(2, 'Baju'),
(4, 'Sepatu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_pembelian`
--

CREATE TABLE `tbl_nota_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_pembelian`
--

INSERT INTO `tbl_nota_pembelian` (`id_pembelian`, `no_nota`, `tanggal`, `id_user`) VALUES
(1, 'NB190920200001', '2020-09-19 00:00:00', '1'),
(2, 'NB190920200002', '2020-09-19 00:00:00', '1'),
(3, 'NB200920200003', '2020-09-20 00:00:00', '1'),
(4, 'NB230920200004', '2020-09-23 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_pembelian_detail`
--

CREATE TABLE `tbl_nota_pembelian_detail` (
  `id_pembelian_detail` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga_satuan` int(11) NOT NULL,
  `diskon` double NOT NULL,
  `subtotal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_pembelian_detail`
--

INSERT INTO `tbl_nota_pembelian_detail` (`id_pembelian_detail`, `id_pembelian`, `id_barang`, `qty`, `harga_satuan`, `diskon`, `subtotal`) VALUES
(1, 1, 2, 10, 0, 0, 500000),
(2, 1, 3, 10, 0, 0, 5000000),
(3, 1, 4, 10, 0, 0, 1000000),
(4, 1, 5, 10, 0, 0, 10000000),
(5, 2, 2, 50, 0, 0, 2500000),
(6, 2, 4, 20, 0, 0, 2000000),
(8, 3, 2, 10, 0, 0, 500000),
(9, 3, 4, 5, 0, 0, 500000),
(10, 3, 4, 2, 0, 0, 200000),
(11, 4, 5, 14, 0, 0, 14000000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_penjualan`
--

CREATE TABLE `tbl_nota_penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_penjualan`
--

INSERT INTO `tbl_nota_penjualan` (`id_penjualan`, `no_nota`, `tanggal`, `id_user`) VALUES
(4, 'NJ190920200002', '2020-09-19 00:00:00', '1'),
(6, 'NJ230920200003', '2020-09-23 00:00:00', '1'),
(8, 'NJ071020200005', '2020-10-07 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_penjualan_detail`
--

CREATE TABLE `tbl_nota_penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `diskon` int(11) NOT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `harga_satuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_penjualan_detail`
--

INSERT INTO `tbl_nota_penjualan_detail` (`id_penjualan_detail`, `id_penjualan`, `id_barang`, `qty`, `diskon`, `subtotal`, `harga_satuan`) VALUES
(3, 4, 2, 4, 0, 800000, 200000),
(4, 4, 3, 4, 0, 1200000, 300000),
(5, 4, 5, 1, 0, 700000, 700000),
(9, 6, 6, 1, 0, 33000, 33000),
(10, 8, 2, 10, 5, 190000, 20000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales`
--

CREATE TABLE `tbl_sales` (
  `id_sales` int(11) NOT NULL,
  `nama_sales` varchar(200) DEFAULT NULL,
  `no_telepon` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sales`
--

INSERT INTO `tbl_sales` (`id_sales`, `nama_sales`, `no_telepon`, `alamat`) VALUES
(1, 'Calvin', '123123123123', 'Jalan Siwalankerto'),
(2, 'Thierry Horax', '085102878123', 'asdqwdqw12312'),
(8, 'Jamiati', '000000', 'Tuban'),
(9, 'Dian', '111111', 'Tuban'),
(10, 'Fellycia', '222222', 'Tuban'),
(11, 'Putri', '333333', 'Tuban'),
(12, 'Agung', '444444', 'Tuban'),
(13, 'Rositi', '555555', 'Tuban');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(200) DEFAULT NULL,
  `no_telepon` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`id_supplier`, `nama_supplier`, `no_telepon`, `alamat`) VALUES
(1, 'Thierry', '0987654321', 'Jalan Siwalankerto'),
(3, 'Calvin', '058120837128', 'Jalan Raya Waru KM 15'),
(4, '11-Royal', '000000', 'Surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `jabatan`) VALUES
(1, 'Thierry Horax', 'thierryhorax', NULL, '$2y$10$exAjPUNQbKPZTU81AwBgYOORz/6FtxFBt1MusmSl1zVTYReyO/yqS', NULL, '2020-09-16 19:58:11', '2020-09-16 19:58:11', 'owner'),
(23, 'Agung', 'devkasirku', NULL, '$2y$10$OHuSbd4BD80x7vJO1UhtF.B9EC2AqXShx0oaoUb.7eIFqCX2L12ka', NULL, '2020-09-17 03:59:24', '2020-09-17 03:59:24', 'Owner'),
(24, 'Jamiati', 'jamiati', NULL, '$2y$10$UVDkduFh7VwdoUWRfiYCZu4BKF3Bi/saP98/zPt10EE4rt89UNQ.i', NULL, NULL, NULL, 'Sales'),
(25, 'Dian', 'dian', NULL, '$2y$10$1Fqoj5xCO7uI36fCTM3foOBFfDcLKpomWJlD0/Efp1gWyJEb/cqnS', NULL, NULL, NULL, 'Sales'),
(26, 'Fellycia', 'fellycia', NULL, '$2y$10$h096EuUocHV9xN1x9L1l1OkSSN0yjadt66IPfOu/WdeQh9V05G8ja', NULL, NULL, NULL, 'Sales'),
(27, 'Putri', 'putri', NULL, '$2y$10$p8jRoOB3hbp2LnlGTMtEFO064cj1AEmql7v1DU53qnpdmYuXxHG/W', NULL, NULL, NULL, 'Sales'),
(28, 'Agung', 'agung', NULL, '$2y$10$awS6fpnWrb80XDPaHklViOs2Tv7v5oVehMZooAY1pqeSGd001wwIu', NULL, NULL, NULL, 'Sales'),
(29, 'Rositi', 'rositi', NULL, '$2y$10$wI7RPLqzoMWCYUsLpqhM/eVsgAw1VsDYemLlhSkbCAm20/qHhIbeu', NULL, NULL, NULL, 'Sales');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `fk_tbl_barang_tbl_kategori1_idx` (`id_kategori`),
  ADD KEY `fk_tbl_barang_tbl_supplier1_idx` (`id_supplier`);

--
-- Indexes for table `tbl_inventory`
--
ALTER TABLE `tbl_inventory`
  ADD PRIMARY KEY (`id_inventory`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_nota_pembelian`
--
ALTER TABLE `tbl_nota_pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_detail`,`id_pembelian`,`id_barang`),
  ADD KEY `fk_tbl_pembelian_has_tbl_barang_tbl_barang1_idx` (`id_barang`),
  ADD KEY `fk_tbl_pembelian_has_tbl_barang_tbl_pembelian_idx` (`id_pembelian`);

--
-- Indexes for table `tbl_nota_penjualan`
--
ALTER TABLE `tbl_nota_penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  ADD PRIMARY KEY (`id_penjualan_detail`,`id_penjualan`,`id_barang`),
  ADD KEY `fk_tbl_penjualan_has_tbl_barang_tbl_barang1_idx` (`id_barang`),
  ADD KEY `fk_tbl_penjualan_has_tbl_barang_tbl_penjualan1_idx` (`id_penjualan`);

--
-- Indexes for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_inventory`
--
ALTER TABLE `tbl_inventory`
  MODIFY `id_inventory` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_nota_pembelian`
--
ALTER TABLE `tbl_nota_pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_nota_penjualan`
--
ALTER TABLE `tbl_nota_penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  MODIFY `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD CONSTRAINT `fk_tbl_barang_tbl_kategori1` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_barang_tbl_supplier1` FOREIGN KEY (`id_supplier`) REFERENCES `tbl_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  ADD CONSTRAINT `fk_tbl_pembelian_has_tbl_barang_tbl_barang1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_pembelian_has_tbl_barang_tbl_pembelian` FOREIGN KEY (`id_pembelian`) REFERENCES `tbl_nota_pembelian` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  ADD CONSTRAINT `fk_tbl_penjualan_has_tbl_barang_tbl_barang1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_penjualan_has_tbl_barang_tbl_penjualan1` FOREIGN KEY (`id_penjualan`) REFERENCES `tbl_nota_penjualan` (`id_penjualan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
