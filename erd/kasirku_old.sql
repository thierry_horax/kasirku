-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 07:48 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasirku`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1d40fcb29adc218e61121f8cc0e1a3b66cc9d0e7e0ae7548f9aa3ed690839055fe2c8421d4a80868', 1, 5, 'thierryhorax@gmail.com', '[]', 0, '2020-09-13 10:19:59', '2020-09-13 10:19:59', '2021-09-13 17:19:59'),
('203b90041e42476f1c563f4027fd110df517f3565ba60f72aa3b93f5704158e3f5fdc1f86463709e', 1, 5, 'thierryhorax@gmail.com', '[]', 0, '2020-09-13 10:40:40', '2020-09-13 10:40:40', '2021-09-13 17:40:40'),
('6710cf3eeca69b94961472dcc5701e76bc7b0ae9a5c79f9bcaea62ba35e6805b2e6077b2aafca9cd', 1, 5, 'thierryhorax@gmail.com', '[]', 0, '2020-09-13 10:33:35', '2020-09-13 10:33:35', '2021-09-13 17:33:35'),
('91ce5d2f8ec83e8593a243d94609a86653459156d090800662e4151891eccecbb17aba4104fa57cf', 1, 5, 'thierryhorax@gmail.com', '[]', 0, '2020-09-14 08:23:47', '2020-09-14 08:23:47', '2021-09-14 15:23:47'),
('ad6c7445b92b64b7caac6b15458f7d928a3baec19ec83c83093e57ba6539535a219d476713f8d534', 1, 1, 'thierryhorax@gmail.com', '[]', 0, '2020-09-04 04:03:36', '2020-09-04 04:03:36', '2021-09-04 11:03:36'),
('adcddf0c11611295b844ba33cce3d30fc26d587c512c630a0d6b6a140d7aa318803bee7efc2104bd', 1, 5, 'thierryhorax@gmail.com', '[]', 0, '2020-09-14 06:56:09', '2020-09-14 06:56:09', '2021-09-14 13:56:09'),
('eae8cbb001cbab540872e0e99bfbd19431e58b693a40bdd7795b0fe0e28b8b52014c975ebe8ee9b5', 1, 3, 'thierryhorax@gmail.com', '[]', 0, '2020-09-13 10:14:50', '2020-09-13 10:14:50', '2021-09-13 17:14:50'),
('f41046b67a545f2736c9d0a4e0f1bc869f24228fa0a94687fa25df12e18ece9907606f6b6bb175bd', 1, 5, 'thierryhorax@gmail.com', '[]', 0, '2020-09-14 07:09:30', '2020-09-14 07:09:30', '2021-09-14 14:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'pBoIzCKzCMOcHf8bEtBXSk9aOH0AyNSxE1OsI50L', NULL, 'http://localhost', 1, 0, 0, '2020-09-04 04:03:23', '2020-09-04 04:03:23'),
(2, NULL, 'Laravel Password Grant Client', '1s5fteJ2AmUJbE41ab6TzLsgBCgi2pZ30afLLn6L', 'users', 'http://localhost', 0, 1, 0, '2020-09-04 04:03:23', '2020-09-04 04:03:23'),
(3, NULL, 'Laravel Personal Access Client', 'nNqRTw2YkR555cycISOOoUm2Yq2MKm60VclIlUGA', NULL, 'http://localhost', 1, 0, 0, '2020-09-13 09:53:04', '2020-09-13 09:53:04'),
(4, NULL, 'Laravel Password Grant Client', 'mofVWPqRJgVKFDt6YKXMAwg77YBmYu5GVUHbvvRJ', 'users', 'http://localhost', 0, 1, 0, '2020-09-13 09:53:04', '2020-09-13 09:53:04'),
(5, NULL, 'Laravel Personal Access Client', 'Q4SLTVEcqr00KzJH98wTAlv8Fh31Rt4nPtuF9w5h', NULL, 'http://localhost', 1, 0, 0, '2020-09-13 10:19:39', '2020-09-13 10:19:39'),
(6, NULL, 'Laravel Password Grant Client', 'NvTB9NwrXy76wue7JDfjuB4VuPjiPD5IwWRHCrq3', 'users', 'http://localhost', 0, 1, 0, '2020-09-13 10:19:39', '2020-09-13 10:19:39');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-04 04:03:23', '2020-09-04 04:03:23'),
(2, 3, '2020-09-13 09:53:04', '2020-09-13 09:53:04'),
(3, 5, '2020-09-13 10:19:39', '2020-09-13 10:19:39');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(45) DEFAULT NULL,
  `nama_barang` varchar(200) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `ongkos_kirim` int(11) DEFAULT NULL,
  `komisi` double DEFAULT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `kode_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `ongkos_kirim`, `komisi`, `id_kategori`, `id_supplier`) VALUES
(2, 'BAJU0001', 'Baju Sekolah', 20000, 50000, 12000, 0.5, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`) VALUES
(2, 'Baju'),
(4, 'Sepatu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_pembelian`
--

CREATE TABLE `tbl_nota_pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_pembelian`
--

INSERT INTO `tbl_nota_pembelian` (`id_pembelian`, `no_nota`, `tanggal`, `id_user`) VALUES
(1, '140920200001', '2020-09-14 00:00:00', '1'),
(2, '140920200002', '2020-09-14 00:00:00', '1'),
(3, '140920200003', '1970-01-01 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_pembelian_detail`
--

CREATE TABLE `tbl_nota_pembelian_detail` (
  `id_pembelian_detail` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_pembelian_detail`
--

INSERT INTO `tbl_nota_pembelian_detail` (`id_pembelian_detail`, `id_pembelian`, `id_barang`, `qty`, `subtotal`) VALUES
(1, 2, 2, 12, 240000),
(6, 3, 2, 5, 100000),
(7, 3, 2, 4, 80000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_penjualan`
--

CREATE TABLE `tbl_nota_penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_penjualan`
--

INSERT INTO `tbl_nota_penjualan` (`id_penjualan`, `no_nota`, `tanggal`, `id_user`) VALUES
(2, 'NJ140920200002', '2020-09-16 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nota_penjualan_detail`
--

CREATE TABLE `tbl_nota_penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nota_penjualan_detail`
--

INSERT INTO `tbl_nota_penjualan_detail` (`id_penjualan_detail`, `id_penjualan`, `id_barang`, `qty`, `subtotal`) VALUES
(2, 2, 2, 2, 40000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales`
--

CREATE TABLE `tbl_sales` (
  `id_sales` int(11) NOT NULL,
  `nama_sales` varchar(200) DEFAULT NULL,
  `no_telepon` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sales`
--

INSERT INTO `tbl_sales` (`id_sales`, `nama_sales`, `no_telepon`, `alamat`) VALUES
(1, 'Calvin', '123123123123', 'Jalan Siwalankerto'),
(2, 'Thierry Horax', '085102878123', 'asdqwdqw12312'),
(3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(200) DEFAULT NULL,
  `no_telepon` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`id_supplier`, `nama_supplier`, `no_telepon`, `alamat`) VALUES
(1, 'Thierry', '0987654321', 'Jalan Siwalankerto');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Thierry Horax', 'thierryhorax@gmail.com', NULL, '$2y$10$LwNVkQpYTeoqPPnq1nwmauWETpHW3MA0NRJXwjYrV3QhOtqs2itK2', NULL, '2020-09-04 04:01:39', '2020-09-04 04:01:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `fk_tbl_barang_tbl_kategori1_idx` (`id_kategori`),
  ADD KEY `fk_tbl_barang_tbl_supplier1_idx` (`id_supplier`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_nota_pembelian`
--
ALTER TABLE `tbl_nota_pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_detail`,`id_pembelian`,`id_barang`),
  ADD KEY `fk_tbl_pembelian_has_tbl_barang_tbl_barang1_idx` (`id_barang`),
  ADD KEY `fk_tbl_pembelian_has_tbl_barang_tbl_pembelian_idx` (`id_pembelian`);

--
-- Indexes for table `tbl_nota_penjualan`
--
ALTER TABLE `tbl_nota_penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  ADD PRIMARY KEY (`id_penjualan_detail`,`id_penjualan`,`id_barang`),
  ADD KEY `fk_tbl_penjualan_has_tbl_barang_tbl_barang1_idx` (`id_barang`),
  ADD KEY `fk_tbl_penjualan_has_tbl_barang_tbl_penjualan1_idx` (`id_penjualan`);

--
-- Indexes for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_nota_pembelian`
--
ALTER TABLE `tbl_nota_pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_nota_penjualan`
--
ALTER TABLE `tbl_nota_penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  MODIFY `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD CONSTRAINT `fk_tbl_barang_tbl_kategori1` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_barang_tbl_supplier1` FOREIGN KEY (`id_supplier`) REFERENCES `tbl_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_nota_pembelian_detail`
--
ALTER TABLE `tbl_nota_pembelian_detail`
  ADD CONSTRAINT `fk_tbl_pembelian_has_tbl_barang_tbl_barang1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_pembelian_has_tbl_barang_tbl_pembelian` FOREIGN KEY (`id_pembelian`) REFERENCES `tbl_nota_pembelian` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_nota_penjualan_detail`
--
ALTER TABLE `tbl_nota_penjualan_detail`
  ADD CONSTRAINT `fk_tbl_penjualan_has_tbl_barang_tbl_barang1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_penjualan_has_tbl_barang_tbl_penjualan1` FOREIGN KEY (`id_penjualan`) REFERENCES `tbl_nota_penjualan` (`id_penjualan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
