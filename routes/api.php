<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// MASUKKAN ROUTE API DISINI. FORMAT SAMA DENGAN ROUTE BIASA TAPI TAMBAHKAN "v1". 

Route::group(['middleware' => ['auth:api']], function () {
    // Route::get("v1/item/list-item", "ItemsController@getListItem")->name("item.listitem")

    //BARANG
    Route::post('v1/barang/store', 'BarangController@store')->name('barang.store');
    Route::post('v1/barang/{idBarang}/update', 'BarangController@update')->name('barang.update');
    Route::post('v1/barang/{idBarang}/destroy', 'BarangController@destroy')->name('barang.destroy');
    Route::get('v1/barang/getallbarang', 'BarangController@getAllBarang')->name('barang.getallbarang');
    Route::get('v1/barang/{idBarang}/getbarangdetail', 'BarangController@getBarangDetail')->name('barang.getbarangdetail');
    Route::post('v1/barang/caribarang', 'BarangController@cariBarang')->name('barang.caribarang');

    //SUPPLIER
    Route::post('v1/supplier/store', 'SupplierController@store')->name('supplier.store');
    Route::post('v1/supplier/{idSupplier}/update', 'SupplierController@update')->name('supplier.update');
    Route::post('v1/supplier/{idSupplier}/destroy', 'SupplierController@destroy')->name('supplier.destroy');
    Route::get('v1/supplier/getallsupplier', 'SupplierController@getAllSupplier')->name('supplier.getallsupplier');
    Route::get('v1/supplier/{idSupplier}/getsupplierdetail', 'SupplierController@getSupplierDetail')->name('supplier.getsupplierdetail');

    //SALES
    Route::post('v1/sales/store', 'SalesController@store')->name('sales.store');
    Route::post('v1/sales/{idSales}/update', 'SalesController@update')->name('sales.update');
    Route::post('v1/sales/{idSales}/destroy', 'SalesController@destroy')->name('sales.destroy');
    Route::get('v1/sales/getallsales', 'SalesController@getAllSales')->name('sales.getallsales');
    Route::get('v1/sales/{idSales}/getsalesdetail', 'SalesController@getSalesDetail')->name('sales.getsalesdetail');


    //KATEGORI
    Route::post('v1/kategori/store', 'KategorisController@store')->name('kategori.store');
    Route::post('v1/kategori/{idKategori}/update', 'KategorisController@update')->name('kategori.update');
    Route::post('v1/kategori/{idKategori}/destroy', 'KategorisController@destroy')->name('kategori.destroy');
    Route::get('v1/kategori/getallkategori', 'KategorisController@getAllKategori')->name('kategori.getallkategori');

    //REPORT
    Route::post('v1/report/getreporttransaksi', 'ReportController@getReportTransaksi')->name('report.getreporttransaksi');
    Route::post('v1/report/getreportkeuntungan', 'ReportController@getReportKeuntungan')->name('report.getreportkeuntungan');

    //PEMBELIAN
    Route::post('v1/pembelian/store', 'PembelianController@store')->name('pembelian.store');
    Route::post('v1/pembelian/{noNota}/update', 'PembelianController@update')->name('pembelian.update');
    Route::post('v1/pembelian/{noNota}/destroy', 'PembelianController@destroy')->name('pembelian.destroy');
    Route::post('v1/pembelian/{noNota}/detail', 'PembelianController@getNotaDetail')->name('pembelian.detail');

    //PENJUALAN
    Route::post('v1/penjualan/store', 'PenjualanController@store')->name('penjualan.store');
    Route::post('v1/penjualan/{noNota}/update', 'PenjualanController@update')->name('penjualan.update');
    Route::post('v1/penjualan/{noNota}/destroy', 'PenjualanController@destroy')->name('penjualan.destroy');
    Route::post('v1/penjualan/{noNota}/detail', 'PenjualanController@getNotaDetail')->name('penjualan.detail');

    //USER
    Route::post('v1/user/{id}/update', 'UsersController@update')->name('user.update');
    Route::post('v1/user/{id}/destroy', 'UsersController@destroy')->name('user.destroy');
});
