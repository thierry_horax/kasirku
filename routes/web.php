<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['checkToken', 'auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    //KATEGORI
    Route::get('/kategori', 'KategorisController@index')->name('kategori');
    Route::get('/kategori/create', 'KategorisController@create')->name('addkategori');
    // Route::get('/kategori/store', 'KategorisController@store')->name('storekategori');
    Route::get('/kategori/edit/{id}', 'KategorisController@edit')->name('editkategori');
    // Route::POST('/kategori/update', 'KategorisController@update')->name('updatekategori');
    // Route::get('/kategori/destroy/{id}', 'KategorisController@destroy')->name('hapuskategori');

    //SUPPLIER
    Route::get('/supplier', 'SupplierController@index')->name('supplier');
    Route::get('/supplier/create', 'SupplierController@create')->name('addsupplier');
    // Route::get('/supplier/store', 'SupplierController@store')->name('storesupplier');
    Route::get('/supplier/edit/{id}', 'SupplierController@edit')->name('editsupplier');

    //SALES
    Route::get('/sales', 'SalesController@index')->name('sales');
    Route::get('/sales/create', 'SalesController@create')->name('addsales');
    // Route::get('/sales/store', 'SalesController@store')->name('storesales');
    Route::get('/sales/edit/{id}', 'SalesController@edit')->name('editsales');

    //BARANG
    Route::get('/barang', 'BarangController@index')->name('barang');
    Route::get('/barang/create', 'BarangController@create')->name('addbarang');
    Route::get('/barang/edit/{id}', 'BarangController@edit')->name('editbarang');

    //PENJUALAN
    Route::get('/penjualan', 'PenjualanController@index')->name('penjualan.index');
    Route::get('/penjualan/create', 'PenjualanController@create')->name('penjualan.create');
    // Route::get('/penjualan', 'PenjualanController@index')->name('penjualan.index');

    //PEMBELIAN
    Route::get('/pembelian', 'PembelianController@index')->name('pembelian.index');
    Route::get('/pembelian/create', 'PembelianController@create')->name('pembelian.create');
    Route::get('/pembelian/{noNota}/edit', 'PembelianController@edit')->name('pembelian.edit');
    Route::get('/pembelian/{noNota}/print', 'PembelianController@print')->name('pembelian.print');

    //PENJUALAN
    Route::get('/penjualan', 'PenjualanController@index')->name('penjualan.index');
    Route::get('/penjualan/create', 'PenjualanController@create')->name('penjualan.create');
    Route::get('/penjualan/{noNota}/edit', 'PenjualanController@edit')->name('penjualan.edit');
    Route::get('/penjualan/{noNota}/print', 'PenjualanController@print')->name('penjualan.print');

    Route::get('/laporan/laporantransaksi', 'ReportController@indexLaporanTransaksi')->name('report.laporantransaksi');
    Route::get('/laporan/laporankeuntungan', 'ReportController@indexLaporanKeuntungan')->name('report.laporankeuntungan');

    Route::get('/user', 'UsersController@index')->name('user.index');
    Route::get('/user/create', 'UsersController@create')->name('user.create');
    Route::get('/user/{id}/edit', 'UsersController@edit')->name('user.edit');
});

Auth::routes();
