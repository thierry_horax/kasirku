<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class checkToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oauth_token = DB::table('oauth_access_tokens as oat')
            ->join('oauth_clients as oc', 'oat.client_id', '=', 'oc.id')
            ->where("oat.user_id", Auth::user()->id)
            ->count();

        if ($oauth_token == 0) {
            Auth::logout();

            return redirect()->route('login');
        } else {
            return $next($request);
        }
        // $http = new Client();
        // $response = $http->post('/oauth/token', [
        //     'form_params' => [
        //         'grant_type' => Session::get(Auth::user()->email . '_access_token'),
        //         'client_id' => $oauth_token[0]->id,
        //         'client_secret' => $oauth_token[0]->secret,
        //         'code' => $request->code,
        //     ],
        // ]);

        // dd($oauth_token);
    }
}
