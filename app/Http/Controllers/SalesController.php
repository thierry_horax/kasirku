<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = $this->getAllSales()->getData();
        return view('sales.index', ['data' => $data->response]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::table('tbl_sales')
                ->insert([
                    'nama_sales' => $request->namaSales,
                    'no_telepon' => $request->noTelepon,
                    'alamat' => $request->alamat,
                ]);

            $user = DB::table('users')
                ->where('username', strtolower(str_replace(" ", "", $request->namaSales)))
                ->count();

            DB::table('users')
                ->insert([
                    'name' => $request->namaSales,
                    'username' => $user == 0 ? strtolower(str_replace(" ", "", $request->namaSales)) : strtolower(str_replace(" ", "", $request->namaSales) . rand(0, 999)),
                    "password" => Hash::make('12345678'),
                    'jabatan' => "Sales"
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datas = DB::table('tbl_sales')->where('id_sales', $id)->first();
        return view('sales.edit', ['data' => $datas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSales)
    {
        try {
            DB::table('tbl_sales')
                ->where("id_sales", $idSales)
                ->update([
                    'nama_sales' => $request->namaSales,
                    'no_telepon' => $request->noTelepon,
                    'alamat' => $request->alamat,
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSales)
    {
        try {
            $sales = DB::table('tbl_sales as s')
                ->where('id_sales', $idSales)
                ->get();

            // $user = DB::table('users')
            //     ->where('username', strtolower(str_replace(" ", "", $sales[0]->nama_sales)))
            //     ->get();

            DB::table('tbl_sales')
                ->where("id_sales", $idSales)
                ->delete();
            // app(UsersController::class)->destroy($user[0]->id);

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllSales()
    {
        try {
            $listSales = DB::table('tbl_sales')
                ->get();

            return response()->json(['response' => $listSales], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getSalesDetail($idSales)
    {
        try {
            $sales = DB::table('tbl_sales')
                ->where('id_sales', $idSales)
                ->get();

            return response()->json(['response' => $sales], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }
}
