<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = $this->getAllSupplier()->getData();
        return view('supplier.index', ['data'=>$data->response]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::table('tbl_supplier')
                ->insert([
                    'nama_supplier' => $request->namaSupplier,
                    'no_telepon' => $request->noTelepon,
                    'alamat' => $request->alamat,
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $datas = DB::table('tbl_supplier')->where('id_supplier', $id)->first();
        return view('supplier.edit', ['data'=>$datas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSupplier)
    {
        try {
            DB::table('tbl_supplier')
                ->where('id_supplier', $idSupplier)
                ->update([
                    'nama_supplier' => $request->namaSupplier,
                    'no_telepon' => $request->noTelepon,
                    'alamat' => $request->alamat,
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSupplier)
    {
        try {

            $listBarang = DB::table('tbl_barang')
                ->where('id_supplier', $idSupplier)
                ->get();

            if (count($listBarang) == 0) {
                DB::table('tbl_supplier')
                    ->where('id_supplier', $idSupplier)
                    ->delete();
            } else {
                //Fail berarti data supplier terdaftar di data barang. jika dihapus kana merusak data barang
                return response()->json(['response' => "fail"], 200);
            }
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllSupplier()
    {
        try {
            $listSupplier = DB::table('tbl_supplier')
                ->get();

            return response()->json(['response' => $listSupplier], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getSupplierDetail($idSupplier)
    {
        try {
            $supplier = DB::table('tbl_supplier')
                ->where('id_supplier', $idSupplier)
                ->get();

            return response()->json(['response' => $supplier], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }
}
