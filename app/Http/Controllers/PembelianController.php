<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listNotaPembelian = $this->getAllNotaPembelian()->getData()->response;
        return view('pembelian.index', compact('listNotaPembelian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $noNota = $this->generateKodeTransaksiPembelian()->getData()->response;
        $listBarang = app(BarangController::class)->getAllBarang()->getData()->response;
        return view('pembelian.create', compact('listBarang', 'noNota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $listBarang = json_decode($request->listBarang, true);
            DB::table('tbl_nota_pembelian')
                ->insert([
                    'no_nota' => $request->noNota,
                    'tanggal' => date("Y-m-d H:i:s", strtotime($request->tanggal)),
                    'id_user' => Auth::user()->id
                ]);

            $lastInsertedNotaPembelian = DB::table('tbl_nota_pembelian')
                ->orderBy('id_pembelian', 'desc')
                ->limit(1)
                ->get();

            $i = 0;
            foreach ($listBarang as $barang) {
                $dbBarang= DB::table('tbl_barang')
                    ->where('id_barang', $barang['idBarang'])
                    ->get();

                DB::table('tbl_nota_pembelian_detail')
                    ->insert([
                        'id_pembelian' => $lastInsertedNotaPembelian[0]->id_pembelian,
                        'id_barang' => $barang['idBarang'],
                        'qty' => $barang['qty'],
                        'subtotal' => $barang['subtotal'],
                        'harga_satuan' => $barang['hargaSatuan'],
                        'diskon' => $barang['diskon'],
                        'satuan' => $barang['satuan']
                    ]);
                    
                for($i; $i<$barang['qty'];$i++)
                {
                    DB::table('tbl_inventory')
                        ->insert([
                            'kode_barang'=> $dbBarang[0]->kode_barang,
                            'qty' => 1,
                            'harga_beli' => $barang['hargaSatuan'],
                            'harga_jual' => null,
                            'tanggal_beli' => $lastInsertedNotaPembelian[0]->tanggal,
                            'tanggal_jual' => null,
                            'status' => "available", 
                            'id_user' => Auth::user()->id,
                            'id_supplier' => $dbBarang[0]->id_supplier,
                            'id_pembelian' => $lastInsertedNotaPembelian[0]->id_pembelian,
                            'id_penjualan' => null,
                            
                        ]);
                }
            }

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($noNota)
    {
        $notaPembelian = DB::table('tbl_nota_pembelian as np')
            ->join('tbl_nota_pembelian_detail as npd', 'npd.id_pembelian', '=', 'np.id_pembelian')
            ->join('tbl_barang as b', 'npd.id_barang', '=', 'b.id_barang')
            ->join('tbl_supplier as s', 's.id_supplier', '=', 'b.id_supplier')
            ->where('np.no_nota', $noNota)
            ->get();
        $listBarang = app(BarangController::class)->getAllBarang()->getData()->response;

        $listBarangInNotaPembelian = array();
        $index = 1;
        foreach ($notaPembelian as $barang) {
            $objectBarang = ['idBarang' => $barang->id_barang, 'qty' => $barang->qty, 'subtotal' => $barang->subtotal, 'nama_supplier' => $barang->nama_supplier, 'index' => $index, 'diskon' => ($barang->diskon == null) ? 0 : $barang->diskon, 'hargaSatuan' => ($barang->harga_satuan == null) ? 0 : $barang->harga_satuan];
            array_push($listBarangInNotaPembelian, $objectBarang);
            $index++;
        }
        return view('pembelian.edit', compact('notaPembelian', 'listBarang', 'listBarangInNotaPembelian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $noNota)
    {
        try {
            $listBarang = json_decode($request->listBarang, true);
            $notaPembelian = DB::table('tbl_nota_pembelian')
                ->where('no_nota', $noNota)
                ->get();

            DB::table('tbl_nota_pembelian')
                ->where('id_pembelian', $notaPembelian[0]->id_pembelian)
                ->update([
                    'tanggal' => date("Y-m-d H:i:s", strtotime($request->tanggal)),
                    'id_user' => Auth::user()->id
                ]);

            DB::table('tbl_nota_pembelian_detail')
                ->where('id_pembelian', $notaPembelian[0]->id_pembelian)
                ->delete();

            foreach ($listBarang as $barang) {
                DB::table('tbl_nota_pembelian_detail')
                    ->insert([
                        'id_pembelian' => $notaPembelian[0]->id_pembelian,
                        'id_barang' => $barang['idBarang'],
                        'qty' => $barang['qty'],
                        'subtotal' => $barang['subtotal'],
                        'harga_satuan' => $barang['hargaSatuan'],
                        'diskon' => $barang['diskon']
                    ]);

            }

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($noNota)
    {
        try {
            $notaPembelian = DB::table('tbl_nota_pembelian')
                ->where('no_nota', $noNota)
                ->get();

            DB::table('tbl_nota_pembelian_detail')
                ->where('id_pembelian', $notaPembelian[0]->id_pembelian)
                ->delete();

            DB::table('tbl_nota_pembelian')
                ->where('id_pembelian', $notaPembelian[0]->id_pembelian)
                ->delete();

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllNotaPembelian()
    {
        try {
            $listNotaPembelian = DB::table('tbl_nota_pembelian as np')
                ->select("np.*", 'u.id', 'u.name')
                ->join('users as u', 'u.id', '=', 'np.id_user')
                ->get();

            return response()->json(['response' => $listNotaPembelian], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function generateKodeTransaksiPembelian()
    {
        try {
            $lastestKodeTransaksi = DB::table("tbl_nota_pembelian")
                ->orderby('id_pembelian', 'desc')
                ->limit(1)
                ->get();

            $kodeTransaksi = "";
            if (count($lastestKodeTransaksi) > 0) {
                $kodeTransaksi = substr($lastestKodeTransaksi[0]->no_nota, -4, 4) + 1;
                $kodeTransaksi = "NB" . date("dmY") . sprintf("%'.04d", $kodeTransaksi);
            } else {
                $kodeTransaksi = "NB" . date('dmY') . "0001";
            }

            return response()->json(['response' => $kodeTransaksi], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getNotaDetail($noNota)
    {
        try {
            $notaDetail = DB::table('tbl_nota_pembelian as np')
                ->join('tbl_nota_pembelian_detail as npd', 'npd.id_pembelian', '=', 'np.id_pembelian')
                ->join('tbl_barang as b', 'b.id_barang', '=', 'npd.id_barang')
                ->join('tbl_supplier as s', 's.id_supplier', '=', 'b.id_supplier')
                ->where('np.no_nota', $noNota)
                ->get();

            return response()->json(['response' => $notaDetail], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function print($noNota)
    {
        try {
            $notaDetail = $this->getNotaDetail($noNota)->getData()->response;
            $total = 0;
            foreach ($notaDetail as $barang) {
                $total += $barang->subtotal;
            }
            return view('print.print_nota_pembelian', compact('notaDetail', 'total'));
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
    }
}
