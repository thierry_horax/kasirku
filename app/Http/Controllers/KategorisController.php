<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategorisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->getAllKategori()->getData();
        return view('kategori.index', ['data' => $data->response]);
        // dd($data->response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::table('tbl_kategori')
                ->insert([
                    'nama_kategori' => $request->namaKategori
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datas = DB::table('tbl_kategori')->where('id_kategori', $id)->first();
        return view('kategori.edit', ['data' => $datas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idKategori)
    {
        try {
            DB::table('tbl_kategori')
                ->where('id_kategori', $idKategori)
                ->update([
                    'nama_kategori' => $request->namaKategori
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idKategori)
    {
        try {
            $kategori = DB::table('tbl_kategori as k')
                ->join('tbl_barang as b', 'b.id_kategori', '=', 'k.id_kategori')
                ->where('k.id_kategori', $idKategori)
                ->count();

            if ($kategori > 0) {
                // return response()->json(['response' => ], 500);
                abort(500, "Barang lain menggunakan kategori tersebut");
            }

            DB::table('tbl_kategori')
                ->where('id_kategori', $idKategori)
                ->delete();
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllKategori()
    {
        try {
            $listKategori = DB::table('tbl_kategori')
                ->get();

            return response()->json(['response' => $listKategori], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }
}
