<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listNotaPenjualan = $this->getAllNotaPenjualan()->getData()->response;
        return view('penjualan.index', compact('listNotaPenjualan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $noNota = $this->generateKodeTransaksiPenjualan()->getData()->response;
        $listBarang = app(BarangController::class)->getAllBarang()->getData()->response;
        return view('penjualan.create', compact('listBarang', 'noNota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $listBarang = json_decode($request->listBarang, true);
            DB::table('tbl_nota_penjualan')
                ->insert([
                    'no_nota' => $this->generateKodeTransaksiPenjualan()->getData()->response,
                    'tanggal' => date("Y-m-d"),
                    'id_user' => Auth::user()->id
                ]);

            $lastInsertedNotaPenjualan = DB::table('tbl_nota_penjualan')
                ->orderBy('id_penjualan', 'desc')
                ->limit(1)
                ->get();

            
            $i = 0;
            foreach ($listBarang as $barang) {
                $dbBarang= DB::table('tbl_barang')
                    ->where('id_barang', $barang['idBarang'])
                    ->get();

                

                DB::table('tbl_nota_penjualan_detail')
                    ->insert([
                        'id_penjualan' => $lastInsertedNotaPenjualan[0]->id_penjualan,
                        'id_barang' => $barang['idBarang'],
                        'qty' => $barang['qty'],
                        'subtotal' => $barang['subtotal'],
                        'harga_satuan' => $barang['hargaSatuan'],
                        'diskon' => $barang['diskon'],
                        'satuan' => $barang['satuan']
                    ]);
                
                $inventory = DB::table('tbl_inventory')
                    ->where('kode_barang', $dbBarang[0]->kode_barang)
                    ->where('tanggal_jual', null)
                    ->orderBy('id_inventory','asc')
                    ->limit($barang['qty'])
                    ->get();

                for($i; $i<$barang['qty'];$i++)
                {
                    DB::table('tbl_inventory')
                        ->where('id_inventory', $inventory[$i]->id_inventory)
                        ->update([
                            'qty' => 1,
                            'harga_jual' => $barang['hargaSatuan'],
                            'tanggal_jual' =>  $lastInsertedNotaPenjualan[0]->tanggal,
                            'status' => "terjual", 
                            'id_penjualan' => $lastInsertedNotaPenjualan[0]->id_penjualan
                        ]);
                }
            }

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($noNota)
    {
        $notaPenjualan = DB::table('tbl_nota_penjualan as np')
            ->join('tbl_nota_penjualan_detail as npd', 'npd.id_penjualan', '=', 'np.id_penjualan')
            ->join('tbl_barang as b', 'npd.id_barang', '=', 'b.id_barang')
            ->where('np.no_nota', $noNota)
            ->get();
        // dd($notaPenjualan);
        $listBarang = app(BarangController::class)->getAllBarang()->getData()->response;

        $listBarangInNotaPenjualan = array();
        $index = 1;
        foreach ($notaPenjualan as $barang) {
            $objectBarang = ['idBarang' => $barang->id_barang, 'qty' => $barang->qty, 'subtotal' => $barang->subtotal, 'index' => $index, 'hargaSatuan' => $barang->harga_satuan, 'diskon' => ($barang->diskon == null) ? 0 : $barang->diskon];
            array_push($listBarangInNotaPenjualan, $objectBarang);
            $index++;
        }
        return view('penjualan.edit', compact('notaPenjualan', 'listBarang', 'listBarangInNotaPenjualan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $noNota)
    {
        try {
            $listBarang = json_decode($request->listBarang, true);
            $notaPenjualan = DB::table('tbl_nota_penjualan')
                ->where('no_nota', $noNota)
                ->get();

            DB::table('tbl_nota_penjualan')
                ->where('id_penjualan', $notaPenjualan[0]->id_penjualan)
                ->update([
                    'tanggal' => date("Y-m-d H:i:s", strtotime($request->tanggal)),
                    'id_user' => Auth::user()->id
                ]);

            DB::table('tbl_nota_penjualan_detail')
                ->where('id_penjualan', $notaPenjualan[0]->id_penjualan)
                ->delete();

                $i = 0;
            foreach ($listBarang as $barang) {
                $dbBarang= DB::table('tbl_barang')
                    ->where('id_barang', $barang['idBarang'])
                    ->get();

                DB::table('tbl_nota_penjualan_detail')
                    ->insert([
                        'id_penjualan' => $notaPenjualan[0]->id_penjualan,
                        'id_barang' => $barang['idBarang'],
                        'qty' => $barang['qty'],
                        'subtotal' => $barang['subtotal'],
                        'harga_satuan' => $barang['hargaSatuan'],
                        'diskon' => $barang['diskon']
                    ]);

                for($i; $i<$barang['qty'];$i++)
                {
                    DB::table('tbl_inventory')
                        ->where('kode_barang', $dbBarang[0]->kode_barang)
                        ->update([
                            'kode_barang'=> $dbBarang[0]->kode_barang,
                            'qty' => 1,
                            'harga_jual' => $barang['hargaSatuan'],
                            'tanggal_jual' =>  $notaPenjualan[0]->tanggal,
                            'status' => "terjual", 
                            'id_user' => Auth::user()->id,
                            'id_supplier' => $dbBarang[0]->id_supplier,
                            'id_penjualan' => $notaPenjualan[0]->id_penjualan
                        ]);
                }
            }

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($noNota)
    {
        try {
            $notaPenjualan = DB::table('tbl_nota_penjualan')
                ->where('no_nota', $noNota)
                ->get();

            DB::table('tbl_nota_penjualan_detail')
                ->where('id_penjualan', $notaPenjualan[0]->id_penjualan)
                ->delete();

            DB::table('tbl_nota_penjualan')
                ->where('id_penjualan', $notaPenjualan[0]->id_penjualan)
                ->delete();

            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function generateKodeTransaksiPenjualan()
    {
        try {
            $lastestKodeTransaksi = DB::table("tbl_nota_penjualan")
                ->orderby('id_penjualan', 'desc')
                ->limit(1)
                ->get();

            $kodeTransaksi = "";
            if (count($lastestKodeTransaksi) > 0) {
                $kodeTransaksi = substr($lastestKodeTransaksi[0]->no_nota, -4, 4) + 1;
                $kodeTransaksi = "NJ" . date("dmY") . sprintf("%'.04d", $kodeTransaksi);
            } else {
                $kodeTransaksi = "NJ" . date('dmY') . "0001";
            }

            return response()->json(['response' => $kodeTransaksi], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllNotaPenjualan()
    {
        try {
            $listNotaPenjualan = DB::table('tbl_nota_penjualan as np')
                ->select("np.*", 'u.id', 'u.name')
                ->join('users as u', 'u.id', '=', 'np.id_user')
                ->get();

            return response()->json(['response' => $listNotaPenjualan], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getNotaDetail($noNota)
    {
        try {
            $notaDetail = DB::table('tbl_nota_penjualan as np')
                ->join('tbl_nota_penjualan_detail as npd', 'npd.id_penjualan', '=', 'np.id_penjualan')
                ->join('tbl_barang as b', 'b.id_barang', '=', 'npd.id_barang')
                ->where('np.no_nota', $noNota)
                ->get();

            return response()->json(['response' => $notaDetail], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function print($noNota)
    {
        try {
            $notaDetail = $this->getNotaDetail($noNota)->getData()->response;
            $total = 0;
            foreach ($notaDetail as $barang) {
                $total += $barang->subtotal;
            }
            return view('print.print_nota_penjualan', compact('notaDetail', 'total'));
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
    }
}
