<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listUser = $this->getAllUser()->getData()->response;
        return view('user.index', compact('listUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = DB::table('users')
                ->where('id', $id)
                ->get();

            return view('user.edit', compact('user'));
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::table('users')
                ->where('id', $id)
                ->update([
                    'name' => $request->name,
                    'username' => $request->username,
                    'jabatan' => $request->jabatan
                ]);

            return response()->json(['response'], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::table('oauth_access_tokens')
                ->where('user_id', $id)
                ->delete();

            DB::table('users')
                ->where('id', $id)
                ->update([
                    'password' => ""
                ]);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllUser()
    {
        try {
            $listUser = DB::table('users')
                ->select("id", 'name', "username", 'jabatan')
                ->where("password", '<>', '')
                ->get();

            return response()->json(['response' => $listUser], 200);
        } catch (\Throwable $th) {
            abort($th->getMessage());
        }
    }
}
