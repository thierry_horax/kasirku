<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexLaporanTransaksi()
    {
        return view('laporan.laporan_transaksi');
    }
    public function indexLaporanKeuntungan()
    {
        return view('laporan.laporan_keuntungan');
    }

    public function getReportTransaksi(Request $request)
    {
        try {
            $tanggalAwal = date('Y-m-d', strtotime($request->tanggalAwal));
            $tanggalAkhir = date('Y-m-d', strtotime($request->tanggalAkhir));
            $report = [];
            $reportNotaPenjualan = DB::select("
                select b.id_barang, b.kode_barang, b.nama_barang, sum(npd.qty) as qty_terjual,  COALESCE(null,0) as qty_terbeli
                from tbl_nota_penjualan as np
                inner join tbl_nota_penjualan_detail as npd on np.id_penjualan = npd.id_penjualan
                inner join tbl_barang as b on npd.id_barang = b.id_barang
                where np.tanggal between '$tanggalAwal' and '$tanggalAkhir'
                group by b.id_barang 
            ");

            $reportNotaPembelian = DB::select("
                select b.id_barang, b.kode_barang, b.nama_barang, COALESCE(null,0) as qty_terjual, sum(npd.qty) as qty_terbeli
                from tbl_nota_pembelian as np
                inner join tbl_nota_pembelian_detail as npd on np.id_pembelian = npd.id_pembelian
                inner join tbl_barang as b on npd.id_barang = b.id_barang
                where np.tanggal between '$tanggalAwal' and '$tanggalAkhir'
                group by b.id_barang
            ");

            foreach ($reportNotaPembelian as $barangTerbeli) {
                $not_exist = false;
                foreach ($reportNotaPenjualan as $barangTerjual) {
                    if ($barangTerbeli->id_barang == $barangTerjual->id_barang) {
                        $not_exist = false;
                        array_push($report, ['id_barang' => $barangTerbeli->id_barang, 'kode_barang' => $barangTerbeli->kode_barang, 'nama_barang' => $barangTerbeli->nama_barang, 'qty_terbeli' => $barangTerbeli->qty_terbeli, 'qty_terjual' => $barangTerjual->qty_terjual]);
                        break;
                    } else {
                        $not_exist = true;
                    }
                }
                if ($not_exist == true) {
                    array_push($report, ['id_barang' => $barangTerbeli->id_barang, 'kode_barang' => $barangTerbeli->kode_barang, 'nama_barang' => $barangTerbeli->nama_barang, 'qty_terbeli' => $barangTerbeli->qty_terbeli, 'qty_terjual' => 0]);
                }
            }

            return response()->json(['response' => $report], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getReportKeuntungan(Request $request)
    {
        try {
            $tanggalAwal = date('Y-m-d', strtotime($request->tanggalAwal));
            $tanggalAkhir = date('Y-m-d', strtotime($request->tanggalAkhir));
            $report = [];
            //JUAL BARANG SELECT SUM SUBTOTAL DARI TIAP BARANG
            // $reportNotaPenjualan = DB::select("
            //     select b.id_barang, b.kode_barang, b.nama_barang, sum(npd.subtotal) as subtot_terjual,  COALESCE(null,0) as subtot_terbeli
            //     from tbl_nota_penjualan as np
            //     inner join tbl_nota_penjualan_detail as npd on np.id_penjualan = npd.id_penjualan
            //     inner join tbl_barang as b on npd.id_barang = b.id_barang
            //     where np.tanggal between '$tanggalAwal' and '$tanggalAkhir'
            //     group by b.id_barang 
            // ");
            //BELI BARANG SELECT SUM SUBTOTAL DARI TIAP BARANG
            // $reportNotaPembelian = DB::select("
            //     select b.id_barang, b.kode_barang, b.nama_barang, COALESCE(null,0) as subtot_terjual, sum(npd.subtotal) as subtot_terbeli
            //     from tbl_nota_pembelian as np
            //     inner join tbl_nota_pembelian_detail as npd on np.id_pembelian = npd.id_pembelian
            //     inner join tbl_barang as b on npd.id_barang = b.id_barang
            //     where np.tanggal between '$tanggalAwal' and '$tanggalAkhir'
            //     group by b.id_barang
            // ");

            // foreach ($reportNotaPembelian as $barangTerbeli) {
            //     $not_exist = false;
            //     foreach ($reportNotaPenjualan as $barangTerjual) {
            //         if ($barangTerbeli->id_barang == $barangTerjual->id_barang) {
            //             $not_exist = false;
            //             array_push($report, ['id_barang' => $barangTerbeli->id_barang, 'kode_barang' => $barangTerbeli->kode_barang, 'nama_barang' => $barangTerbeli->nama_barang, 'subtot_terbeli' => $barangTerbeli->subtot_terbeli, 'subtot_terjual' => $barangTerjual->subtot_terjual]);
            //             break;
            //         } else {
            //             $not_exist = true;
            //         }
            //     }
            //     if ($not_exist == true) {
            //         array_push($report, ['id_barang' => $barangTerbeli->id_barang, 'kode_barang' => $barangTerbeli->kode_barang, 'nama_barang' => $barangTerbeli->nama_barang, 'subtot_terbeli' => $barangTerbeli->subtot_terbeli, 'subtot_terjual' => 0]);
            //     }
            // }

            //INI QUERY REPORT PROFIT
            $query = DB::select("
                select *
                from tbl_inventory
                where tanggal_jual between '$tanggalAwal' and '$tanggalAkhir'
            ");

            $query2 = DB::select("
            select np.no_nota, i.tanggal_jual, b.nama_barang, count(*) as qty, i.harga_beli * npd.qty as harga_beli, i.harga_jual * npd.qty as harga_jual, (i.harga_jual-i.harga_beli) * npd.qty as labarugi, npd.satuan as satuan
            from tbl_inventory i INNER JOIN tbl_barang b on i.kode_barang = b.kode_barang
            INNER JOIN tbl_nota_penjualan np on i.id_penjualan = np.id_penjualan
            INNER JOIN tbl_nota_penjualan_detail npd on np.id_penjualan = npd.id_penjualan
            WHERE tanggal_jual BETWEEN '2020-10-01' and '2020-10-10'
            group by i.kode_barang, i.tanggal_beli");

            foreach ($query2 as $data){
                array_push($report, [
                    'no_nota' => $data->no_nota, 
                    'tanggal' => $data->tanggal_jual, 
                    'nama_barang' => $data->nama_barang,
                    'satuan' => $data->satuan,
                    'qty' => $data->qty,
                    'harga_jual' => $data->harga_jual,
                    'harga_beli' => $data->harga_beli]);
            }

            return response()->json(['response' => $report], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }
}
