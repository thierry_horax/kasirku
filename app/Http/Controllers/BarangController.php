<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = $this->getAllBarang()->getData()->response;
        return view('barang.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategori = DB::table('tbl_kategori')->get();
        $supplier = DB::table('tbl_supplier')->get();
        return view('barang.create', ['kategoris' => $kategori, 'suppliers' => $supplier]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::table('tbl_barang')
                ->insert([
                    'kode_barang' => $request->kodeBarang,
                    'nama_barang' => $request->namaBarang,
                    'harga_beli' => (int) $request->hargaBeli,
                    'harga_jual' => (int) $request->hargaJual,
                    'ongkos_kirim' => (int) $request->ongkosKirim,
                    'komisi' => (float)$request->komisi,
                    'id_kategori' => $request->idKategori,
                    'id_supplier' => $request->idSupplier
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategori = DB::table('tbl_kategori')->get();
        $supplier = DB::table('tbl_supplier')->get();
        $datas = DB::table('tbl_barang')->where('id_barang', $id)->first();
        return view('barang.edit', ['data' => $datas, 'kategoris' => $kategori, 'suppliers' => $supplier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idBarang)
    {
        try {
            DB::table('tbl_barang')
                ->where('id_barang', $idBarang)
                ->update([
                    'kode_barang' => $request->kodeBarang,
                    'nama_barang' => $request->namaBarang,
                    'harga_beli' => (int) $request->hargaBeli,
                    'harga_jual' => (int) $request->hargaJual,
                    'ongkos_kirim' => (int) $request->ongkosKirim,
                    'komisi' => (float)$request->komisi,
                    'id_kategori' => $request->idKategori,
                    'id_supplier' => $request->idSupplier
                ]);
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idBarang)
    {
        try {
            $listBarangDiPenjualan = DB::table('tbl_nota_penjualan_detail')
                ->where('id_barang', $idBarang)
                ->get();

            $listBarangDiPembelian = DB::table('tbl_nota_pembelian_detail')
                ->where('id_barang', $idBarang)
                ->get();

            if (count($listBarangDiPenjualan) == 0 && count($listBarangDiPembelian) == 0) {
                DB::table('tbl_barang')
                    ->where('id_barang', $idBarang)
                    ->delete();
            } else {
                //Fail berarti barang terdaftar di penjualan dan pembelian. jika dihapus kana merusak data penjualan dan pembelian
                return response()->json(['response' => "fail"], 200);
            }
            return response()->json(['response' => "success"], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getAllBarang()
    {
        try {
            $listBarang = DB::table('tbl_barang as b')
                ->join('tbl_supplier as s', 's.id_supplier', '=', 'b.id_supplier')
                ->join('tbl_kategori as k', 'k.id_kategori', '=', 'b.id_kategori')
                ->get();


            return response()->json(['response' => $listBarang], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function getBarangDetail($idBarang)
    {
        try {
            $barang = DB::table('tbl_barang as b')
                ->join('tbl_supplier as s', 's.id_supplier', '=', 'b.id_supplier')
                ->join('tbl_kategori as k', 'k.id_kategori', '=', 'b.id_kategori')
                ->where("id_barang", $idBarang)
                ->get();


            return response()->json(['response' => $barang], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }

    public function cariBarang(Request $request)
    {
        try {
            $barang = DB::table('tbl_barang as b')
                ->join('tbl_supplier as s', 's.id_supplier', '=', 'b.id_supplier')
                ->join('tbl_kategori as k', 'k.id_kategori', '=', 'b.id_kategori')
                ->where("b.kode_barang", $request->kodeBarang)
                ->get();


            return response()->json(['response' => $barang], 200);
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }
    }
}
